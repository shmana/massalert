@QDA-2031
Feature: 501 Create MA with only MY groups

	Background:
		#@PRECOND_QDA-2030
		Given sign in opened page
		When user entered his username "massalert" and password "qwe123123"
		When user click on Log In button
		Then user should be routed to home page

	@TEST_QDA-2029
	Scenario: 501 Create MA with only MY groups
		Given displaying the automated user
		      When user click to MassAlerts icon
		
		      When user click to create MassAlert icon
		      And user enter the MassAlert Name
		      And user enter the MassAlert Description
		      And user enter the MassAlert Pin code
		        Then verify displaying the "Create MassAlert" text
		        And verify displaying the Information tab
		        And verify displaying the Internal tab
		        And verify displaying the External tab
		        And verify displaying the QBOX tab
		        And verify displaying the Location tab
		        And verify displaying the API's tab
		        And verify displaying the Next button
		        And verify displaying the Photo block
		        And verify displaying the Change icon link
		        And verify displaying the Name text
		        And verify displaying the Name input
		        And verify displaying the Description text
		        And verify displaying the Description input
		        And verify displaying the Pincode text
		        And verify displaying the Pincode input
		        And verify displaying the Allow Reply text
		        And verify displaying the Allow Reply checkbox
		        And verify displaying the Repeat MassAlert text
		        And verify displaying the Repeat MassAlert checkbox
		        And verify displaying the Allow Acknowledge text
		        And verify displaying the Allow Acknowledge checkbox
		        And verify displaying the Priority Profile text
		        And verify displaying the Priority Profile dropdown list
		      When user click to Priority Profile dropdown list
		        Then verify displaying the Critical option
		        And verify displaying the Warning option
		        And verify displaying the Normal option
		
		    When user click to Internal Tab
		    Then verify displaying the Next button
		      And verify displaying the Paging Group Notification block
		        And verify displaying the On Start text on the Paging Group Notification block
		          And verify displaying the Paging group text for On Start block
		          And verify displaying the Paging group dropdown list for On Start block
		          And verify displaying the My Group text for On Start block
		          And verify displaying the My Group dropdown list for On Start block
		        And verify displaying the On Clear text
		          And verify displaying the Paging group text for On Clear block
		          And verify displaying the Paging group dropdown list for On Clear block
		          And verify displaying the My Group text for On Clear block
		          And verify displaying the My Group dropdown list for On Clear block
		    When user click to Conference Group block
		      Then verify displaying the Conference Group block
		      And verify displaying the Conference Group dropdown list
		      When user click to the Conference Group dropdown list
		      Then verify displaying the no matching option
		    When user click to the DialOut Group Notification block
		      Then verify displaying the DialOut Group Notification block
		      And verify displaying the On Start text on the DialOut Group Notification block
		      And user verify the DialOut Group Notification dropdown list
		      When user click to On Start dropdown list
		      Then verify displaying the no matching option
		
		    When user click to Paging Group Notification block
		      When user click to the Paging group dropdown list on the On Start block
		      And user select the Group for Auto Tests on the On Start block
		        Then verify displaying Message Type text on the On Start block
		        And verify displaying the Message Type dropdown list on the On Start block
		          When user click to the Message type dropdown list on the On Start block
		          Then verify displaying the Text option on the On Start block
		          And verify displaying the Text to Speache option on the On Start block
		          And verify displaying the Text and Text to Speache option on the On Start block
		          And verify displaying the Media file option on the On Start block
		          When user click to the Text option on the On Start block
		        Then verify displaying the Message text on the On Start block
		        And verify displaying the Message input on the On Start block
		        When user enter the Message to the Message input on the On Start block
		
		      When user click to the Paging group dropdown list on the On the Clear Block
		      And user select the Group for Auto Tests On the Clear Block
		        Then verify displaying Message Type text On the Clear Block
		        And verify displaying the Message Type dropdown list On the Clear Block
		          When user click to the Message type dropdown list On the Clear Block
		          Then verify displaying the Text option On the Clear Block
		          And verify displaying the Text to Speache option On the Clear Block
		          And verify displaying the Text and Text to Speache option On the Clear Block
		          And verify displaying the Media file option On the Clear Block
		          When user click to the Text option On the Clear Block
		        Then verify displaying the Message text On the Clear Block
		        And verify displaying the Message input On the Clear Block
		        When  user enter the Message to the Message input On the Clear Block
		
		
		    When user click to External Tab
		      Then verify displaying the Email Addresses text on the External Tab
		      And verify displaying the Enter Email Address input on the External Tab
		      And verify displaying the hint on the External tab on the External Tab
		      And verify displaying the Import link on the External Tab
		      And verify displaying the On Start text on the External Tab
		      And verify displaying the On Start email input on the External Tab
		      And verify displaying the On Clear text on the External Tab
		      And verify displaying the On Clear email input on the External Tab
		
		    When user click to QBOX Tab
		      Then verify displaying the On Start text on the QBOX Tab
		      And verify displaying dropdown list on the On start block on the QBOX Tab
		        When user click to the playlist dropdown on the On start block on the QBOX Tab
		        Then verify displaying the play list on the On start block on the QBOX Tab
		
		      Then verify displaying the On Clear text on the QBOX Tab
		      And verify displaying dropdown list on the On clear block on the QBOX Tab
		        When user click to the playlist dropdown on the On clear block on the QBOX Tab
		        Then verify displaying the play list on the On clear block on the QBOX Tab
		
		    When user click to Location Tab
		      Then verify displaying the Default Location text
		      And verify displaying the hint on the Location page
		      And verify displaying the location dropdown list
		        When user click to the select location dropdown list
		        Then verify displaying the location on the select location dropdown list
		      And verify displaying the select location checkbox on the location page
		      And verify displaying the select location text on the location page
		
		    When user click to API's Tab
		      Then verify displaying the create button
		      And verify displaying the HTTP Notification text
		      And verify displaying the URL text
		      And verify displaying the URL input
		      And verify displaying the Username text
		      And verify displaying the Username input
		      And verify displaying the Password text
		      And verify displaying the Password input
		
		    When user click to create button
		      Then verify displaying the MassAlert Name
		      And verify displaying the MassAlert description
		      And verify displaying the MassAlert image
		      And verify displaying the Start button
		      When user click to the Detail tab
		      Then verify displaying the Detail tab on the created MassAlert
		      When user click to the History tab
		      Then verify displaying the History tab on the created MassAlert
		      When user click to the Share With tab
		      Then verify displaying the Share With tab on the created MassAlert
		      When user click to the Subscribers tab
		      Then verify displaying the Subscribers tab on the created MassAlert
		      When user click to the Detail tab
		      Then verify displaying the Edit button on the created MassAlert
		      And verify displaying the Delete button on the on the created MassAlert
		
		      And verify displaying the Paging group text for On Start block
		      And verify displaying the selected group for On Start block
		      And verify displaying the Message type text for On Start block
		      And verify displaying the selected message text for On Start block
		      And verify displaying the Message text for On Start block
		      And verify displaying the entered message for On Start block
		
		      And verify displaying the Paging group text for On Clear block
		      And verify displaying the selected group for On Clear block
		      And verify displaying the Message type text for On Clear block
		      And verify displaying the selected message text for On Clear block
		      And verify displaying the Message text for On Clear block
		      And verify displaying the entered message for On Clear block
		
		      And verify displaying the created MassAlert on the Mass Alerts tab
