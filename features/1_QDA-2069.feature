@QDA-2070
Feature: 514 search MA

	Background:
		#@PRECOND_QDA-2030
		Given sign in opened page
		When user entered his username "massalert" and password "qwe123123"
		When user click on Log In button
		Then user should be routed to home page
		#@PRECOND_QDA-2034
		Given displaying the automated user
		      When user click to MassAlerts icon
		      And user click to create MassAlert icon
		      And user enter the MassAlert Name
		      And user enter the MassAlert Description
		      And user enter the MassAlert Pin code
		      And user click to Internal Tab
		
		      And user click to the Paging group dropdown list on the On Start block
		      And user select the Group for Auto Tests on the On Start block
		      And user enter the Message to the Message input on the On Start block
		
		      And user click to the Paging group dropdown list on the On the Clear Block
		      And user select the Group for Auto Tests On the Clear Block
		      And user enter the Message to the Message input On the Clear Block
		      And user click to API's Tab
		      And user click to create button
		        Then verify displaying the created MassAlert

	@TEST_QDA-2069
	Scenario: 514 search MA
		Given displaying the automated user
		      When user click to the all tab
		      And user click to the search icon on the MassAlert page
		      And user enter the MassAlert Name to the search input
		        Then verify displaying the MassAlert
		      When user click to the my tab
		      And user enter the MassAlert Name to the search input
		        Then verify displaying the MassAlert
