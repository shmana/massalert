Feature: 509 Scenario outline for message input (Internal tab -> clear block)

   Scenario: Login to the MassAlert page as user 1
     Given sign in opened page
       When user entered his username "massalert" and password "qwe123123"
       When user click on Log In button
         Then user should be routed to home page

   Scenario Outline: user create MassAlert Outline for Message input internal tab clear block
      Given displaying the automated user
        When user click to MassAlerts icon
          And user click to create MassAlert icon
          And user enter the MassAlert Name
          And user enter the MassAlert Description
          And user enter the MassAlert Pin code
          And user click to Internal Tab

          And user click to the Paging group dropdown list on the On Start block
          And user select the Group for Auto Tests on the On Start block
          And user enter the Message to the Message input on the On Start block

          And user click to the Paging group dropdown list on the On the Clear Block
          And user select the Group for Auto Tests On the Clear Block
          And user enter the Message to the Message <message> input On the Clear Block On the Internal Tab
          And user click to API's Tab
          And user click to create button for incorrect outline
            Then verify displaying the error pop up for message
          When user click to close button on the Accepted pop up
          When user click to back button
          And user confirm leave


      Examples:
        | message                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
        | spaces                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
        | 1025charactersabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#$%&! *-_.=@[]'{''}'ÀÂÄÈÉÊËÎÏÔÙÛÜàâäèéêëîïôùûüÿÇç«»ÄäÖöÜüßæÆ\u0152\u01531025charactersabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#$%&! *-_.=@[]'{''}'ÀÂÄÈÉÊËÎÏÔÙÛÜàâäèéêëîïôùûüÿÇç«»ÄäÖöÜüßæÆ\u0152\u01531025charactersabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#$%&! *-_.=@[]'{''}'ÀÂÄÈÉÊËÎÏÔÙÛÜàâäèéêëîïôùûüÿÇç«»ÄäÖöÜüßæÆ\u0152\u01531025charactersabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#$%&! *-_.=@[]'{''}'ÀÂÄÈÉÊËÎÏÔÙÛÜàâäèéêëîïôùûüÿÇç«»ÄäÖöÜüßæÆ\u0152\u01531025charactersabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#$%&! *-_.=@[]'{''}'ÀÂÄÈÉÊËÎÏÔÙÛÜàâäèéêëîïôùûüÿÇç«»ÄäÖöÜüßæÆ\u0152\u01531025charactersabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#$%&! *-_.=@[]'{''}'ÀÂÄÈÉÊËÎÏÔÙÛÜàâäèéêëîïôùûüÿÇç«»ÄäÖöÜüßæÆ\u0152\u01531025charactersabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#$%&! *-_.=@[]'{''}'ÀÂÄÈÉÊËÎÏÔÙÛÜàâäèéêëîïôùûüÿÇç«»ÄäÖöÜüßæÆ\u |
        | empty                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |



