@QDA-2084
Feature: 508 trigger the QR code to subscribe 

	Background:
		#@PRECOND_QDA-2030
		Given sign in opened page
		When user entered his username "massalert" and password "qwe123123"
		When user click on Log In button
		Then user should be routed to home page
		#@PRECOND_QDA-2034
		Given displaying the automated user
		      When user click to MassAlerts icon
		      And user click to create MassAlert icon
		      And user enter the MassAlert Name
		      And user enter the MassAlert Description
		      And user enter the MassAlert Pin code
		      And user click to Internal Tab
		
		      And user click to the Paging group dropdown list on the On Start block
		      And user select the Group for Auto Tests on the On Start block
		      And user enter the Message to the Message input on the On Start block
		
		      And user click to the Paging group dropdown list on the On the Clear Block
		      And user select the Group for Auto Tests On the Clear Block
		      And user enter the Message to the Message input On the Clear Block
		      And user click to API's Tab
		      And user click to create button
		        Then verify displaying the created MassAlert

	@TEST_QDA-2083
	Scenario: 508 trigger the QR code to subscribe 
		Given displaying the automated user
		       When user click to the Share With tab
		      And user click to the QR icon
		        Then verify displaying Share MassAlert subscriber link
		        And verify displaying the QR code image
		        And verify displaying the Mail icon
		        And verify displaying the Print icon
		        And verify displaying the Facebook icon
		        And verify displaying the Linkedin icon
		        And verify displaying the embedded link
		      When user click to the Share MassAlert link
		        Then verify displaying the back button
		          Then verify displaying the Subscribe MassAlert page
		            Then verify displaying the MassAlert name on the Subscribe page
		            Then verify displaying the Internal block text
		            Then verify displaying the Login Id* text
		            Then verify displaying the Login Id input
		            Then verify displaying the Password* text
		            Then verify displaying the Password input for Internal block
		            Then verify displaying the Subscribe button on the Internal block
		          Then verify displaying the External block text
		          Then verify displaying the First name* text
		          Then verify displaying the First name input
		          Then verify displaying the Last name* text
		          Then verify displaying the Last name input
		          Then verify displaying the Email* text
		          Then verify displaying the Email input
		          Then verify displaying the Phone text
		          Then verify displaying the Phone input
		          Then verify displaying the Subscribe button on the External block
		      When user click to back button on the Subscribe page
		    Given displaying the automated user
