Feature: 522 Create MA with different groups -> Start Stop MA from different users

  Scenario: Login to the MassAlert page as user 1
    Given sign in opened page
    When user entered his username "massalert" and password "qwe123123"
    When user click on Log In button
    Then user should be routed to home page

  Scenario: user create MassAlert
    Given displaying the automated user
      When user click to MassAlerts icon
      And user click to create MassAlert icon
      And user enter the MassAlert Name
      And user enter the MassAlert Description
      And user enter the MassAlert Pin code
      And user click to Internal Tab

      And user click to the Paging group dropdown list on the On Start block
      And user select the Group for Auto Tests on the On Start block
      And user enter the Message to the Message input on the On Start block

      And user click to the Paging group dropdown list on the On the Clear Block
      And user select the Group for Auto Tests 1 On the Clear Block
      And user enter the Message to the Message input On the Clear Block

      And user click to API's Tab
      And user click to create button
        Then verify displaying the created MassAlert

    Scenario: user share MassAlert with execute permissions
    Given displaying the automated user
      #When user click to MassAlerts icon
      When user click to selected MassAlert
      And user click to the Share With tab
      And user click to the Share icon
      And user enter the user name
      And user click to the search icon
      And user click to the execute checkbox
        Then verify status checkboxes for execute permissions
      When user click to the share button
        Then verify displaying the shared user

  Scenario: Login to the MassAlert page as user 2
      Given sign in opened page
        When user entered his username "massalert1" and password "qwe123123"
        When user click on Log In button
          Then user should be routed to home page

  Scenario: user accept MassAlert
    Given displaying the automated user
        When user click to Feed icon
        And user click to the accepted message
        And user click to the Accept button
          Then verify displaying the Accepted pop up
        When user click to close button on the Accepted pop up

  Scenario: User Start MassAlert
    Given displaying the automated user
      When user click to MassAlerts icon
      And user click to selected MassAlert
      And user click to start MassAlert
      And user enter the Pin code MassAlert
      And user click to the Start button
        Then verify displaying pop up window about successful started MassAlert
      When user click to the Close button
        Then verify MassAlert has started

  Scenario:Verify displaying Message on the Start group
    Given displaying the automated user
      When user click to Group icon
      And user click to the on Start group (Group for Auto Tests)
      And user click to the Message tab
        Then verify displaying message by Massalert on the Group page
        #Then verify displaying message by Massalert for on Start group on the Group page
      When user click to created message by Massalert on the Group page
      #When user click to created message by Massalert for on Start group on the Group page
        Then verify displaying the Feed page
        And verify displaying message by Massalert for on Start group on the Feed page
        And verify MassAlert has started

  Scenario: Login to the MassAlert page as user 1
    Given sign in opened page
      When user entered his username "massalert" and password "qwe123123"
      When user click on Log In button
        Then user should be routed to home page

  Scenario: User Stop MassAlert
    Given displaying the automated user
      When user click to MassAlerts icon
      And user click to selected MassAlert
      And user click to stop MassAlert
      And user enter the Pin code MassAlert
      And user click to the Stop button
        Then verify MassAlert has finished

  Scenario:Verify displaying Message on the Clear group
    Given displaying the automated user
      When user click to Group icon
      And user click to the on Start group (Group for Auto Tests 2)
      And user click to the Message tab
        Then verify displaying message by Massalert on the Group page
        #Then verify displaying message by Massalert for on Clear group on the Group page
      When user click to created message by Massalert on the Group page
      #When user click to created message by Massalert for on Clear group on the Group page
        Then verify displaying the Feed page
        And verify displaying message by Massalert for on Clear group on the Feed page
        And verify displaying the Massalert icon on Clear group the Feed page