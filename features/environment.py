# -- FILE: features/environment.py
from behave import fixture, use_fixture
from selenium import webdriver
import random

from webdriver_manager.chrome import ChromeDriverManager

@fixture
def selenium_browser_chrome(context):
    # -- HINT: @behave.fixture is similar to @contextlib.contextmanager
    context.browser = webdriver.Chrome(ChromeDriverManager().install())
    context.browser.maximize_window()
    #for _ in range(10):
    #    new_count = random.uniform(0, 5)
    #    print(new_count)

    yield context.browser
    # -- CLEANUP-FIXTURE PART:
    context.browser.quit()


def before_all(context):
    use_fixture(selenium_browser_chrome, context)
