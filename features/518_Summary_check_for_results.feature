Feature: 518 Summary -> check for results

  Scenario: Login to the MassAlert page as user 1
    Given sign in opened page
    When user entered his username "massalert" and password "qwe123123"
    When user click on Log In button
    Then user should be routed to home page

  Scenario: user create MassAlert for check results
    Given displaying the automated user
      When user click to MassAlerts icon
      And user click to create MassAlert icon
      And user enter the MassAlert Name
      And user enter the MassAlert Description
      And user enter the MassAlert Pin code
      And user click to Internal Tab
      And user click to the Paging group dropdown list on the On Start block
      And user select the Group for Auto Tests on the On Start block
      And user enter the Message to the Message input on the On Start block
      And user click to the Paging group dropdown list on the On the Clear Block
      And user select the Group for Auto Tests On the Clear Block
      And user enter the Message to the Message input On the Clear Block
      And user click to API's Tab
      And user click to create button
        Then verify displaying the created MassAlert

  Scenario: User Start MassAlert
    Given displaying the automated user
   # When user click to MassAlerts icon
      When user click to selected MassAlert
      And user click to start MassAlert
      And user enter the Pin code MassAlert
      And user click to the Start button
        Then verify displaying pop up window about successful started MassAlert
      When user click to the Summery button
        Then verify displaying the doughnut chart
     And verify displaying the chart size monitor

