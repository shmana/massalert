Feature: Subscription to MA internal user

  Scenario: Login to the MassAlert page as user 1
    Given sign in opened page
    When user entered his username "massalert" and password "qwe123123"
    When user click on Log In button
    Then user should be routed to home page

  Scenario: user create MassAlert
    Given displaying the automated user
      When user click to MassAlerts icon
      And user click to create MassAlert icon
      And user enter the MassAlert Name
      And user enter the MassAlert Description
      And user enter the MassAlert Pin code
      And user click to Internal Tab
      And user click to the Paging group dropdown list on the On Start block
      And user select the Group for Auto Tests on the On Start block
      And user enter the Message to the Message input on the On Start block
      And user click to the Paging group dropdown list on the On the Clear Block
      And user select the Group for Auto Tests On the Clear Block
      And user enter the Message to the Message input On the Clear Block
      And user click to API's Tab
      And user click to create button
        Then verify displaying the created MassAlert

  Scenario: trigger to QR code
    Given displaying the automated user
    When user click to selected MassAlert
      And user click to the Share With tab
      And user click to the QR icon
      And user click to the Share MassAlert link
        Then verify displaying the subscription page

  Scenario: Subscription page for internal user
      Given verify displaying the Subscribe MassAlert page
        When user enter the First Name
        And user enter the Last Name
        And user enter email
        And user click to the subscribe button for external user
          Then verify displaying the page about success added external user

  Scenario: authorization Google
    Given user user open gmail authorization page
      When user entered the valid mail "Autotests.quicklert@gmail.com" on the gmail authorization page
      And user click to the next button on the gmail authorization page
      And user entered the valid pass "Autotests911" on the gmail authorization page
      And user click to the next button on the gmail authorization page

    Scenario: user verify message on the gmail
      Given user open the gmail mail page
      When user click to the received message
      And user click to confirm link
       Then verify displaying the page about success added external user after verify
########################################################################################################################



