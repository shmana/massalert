Feature: 513 check for a tabs and MA on it


  Scenario: Login to the Massalert page as user 1
    Given sign in opened page
      When user entered his username "massalert" and password "qwe123123"
      When user click on Log In button
      Then user should be routed to home page

  Scenario: check for a tabs and MA on it
    Given displaying the automated user
      When user click to MassAlerts icon
        Then verify displaying MassAlert title
        And verify displaying the create MassAlert icon
        And verify displaying the refresh list icon
        And verify displaying the search icon
        And verify displaying the All block
        And verify displaying the My block
        And verify displaying the Shared block
        And verify displaying the "Please select MassAlert" text
        And verify displaying the Icons and titles on MassAlerts