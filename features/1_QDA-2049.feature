@QDA-2051
Feature: 507 Share MA with edit permission 

	Background:
		#@PRECOND_QDA-2030
		Given sign in opened page
		When user entered his username "massalert" and password "qwe123123"
		When user click on Log In button
		Then user should be routed to home page
		#@PRECOND_QDA-2034
		Given displaying the automated user
		      When user click to MassAlerts icon
		      And user click to create MassAlert icon
		      And user enter the MassAlert Name
		      And user enter the MassAlert Description
		      And user enter the MassAlert Pin code
		      And user click to Internal Tab
		
		      And user click to the Paging group dropdown list on the On Start block
		      And user select the Group for Auto Tests on the On Start block
		      And user enter the Message to the Message input on the On Start block
		
		      And user click to the Paging group dropdown list on the On the Clear Block
		      And user select the Group for Auto Tests On the Clear Block
		      And user enter the Message to the Message input On the Clear Block
		      And user click to API's Tab
		      And user click to create button
		        Then verify displaying the created MassAlert
		#@PRECOND_QDA-2050
		Given displaying the automated user
		      When user click to selected MassAlert
		      And user click to the Share With tab
		      And user click to the Share icon
		      And user enter the user name
		      And user click to the search icon
		      And user click to the edit checkbox
		        Then verify status checkboxes for edit permissions
		      When user click to the share button
		        Then verify displaying the shared user
		#@PRECOND_QDA-2035
		Given sign in opened page
		        When user entered his username "massalert1" and password "qwe123123"
		        When user click on Log In button
		          Then user should be routed to home page
		#@PRECOND_QDA-2039
		Given displaying the automated user
		        When user click to Feed icon
		        And user click to the accepted message
		        And user click to the Accept button
		          Then verify displaying the Accepted pop up
		        When user click to close button on the Accepted pop up

	@TEST_QDA-2049
	Scenario: 507 Share MA with edit permission 
		Given displaying the automated user
		        When user click to MassAlerts icon
		        And user click to the all tab
		        And user click to selected MassAlert
		          Then verify displaying the Details tab
		          And verify displaying the History tab
		          And verify displaying the Shared With tab
		          And verify displaying the Subscribers tab
		          And verify displaying the Start button
		          And verify displaying the Edit button
		          And verify not displaying the Delete button
		        #When user click to MassAlerts icon
		        When user click to the Shared tab
		        And user click to selected MassAlert
		          Then verify displaying the Details tab
		          And verify displaying the History tab
		          And verify displaying the Shared With tab
		          And verify displaying the Subscribers tab
		          And verify displaying the Start button
		          And verify displaying the Edit button
		          And verify not displaying the Delete button
