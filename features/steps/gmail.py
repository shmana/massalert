import time

from MassAlert import *

from behave import *


from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By


@given(u'user user open gmail authorization page')
def load_login_page(context):
    #time.sleep(2)
    context.browser.get("https://accounts.google.com/")
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, "input[type= 'email']")))
    #time.sleep(2000)

@when(u'user entered the valid mail "{mail}" on the gmail authorization page')
def step_impl(context, mail):
    #time.sleep(2)
    context.browser.find_element_by_css_selector('input[type = "email"]').clear()
    assert context.browser.find_element_by_css_selector('input[type = "email"]')
    context.browser.find_element_by_css_selector('input[type = "email"]').send_keys(mail)
    #time.sleep(2)



@when(u'user click to the next button on the gmail authorization page')
def step_impl(context):
    time.sleep(2)
    context.browser.find_element_by_css_selector('button[class="VfPpkd-LgbsSe VfPpkd-LgbsSe-OWXEXe-k8QpJ VfPpkd-LgbsSe-OWXEXe-dgl2Hf nCP5yc AjY5Oe DuMIQc qIypjc TrZEUc lw1w4b"]').click()
    time.sleep(2)

@when(u'user entered the valid pass "{password}" on the gmail authorization page')
def step_impl(context, password):
    time.sleep(1)
    context.browser.find_element_by_css_selector('input[type="password"]').clear()
    assert context.browser.find_element_by_css_selector('input[type = "password"]')
    context.browser.find_element_by_css_selector('input[type = "password"]').send_keys(password)
    #time.sleep(2)

@given(u'user open the gmail mail page')
def step_impl(context):
    context.browser.get("https://mail.google.com/mail/u/0/#inbox")
    time.sleep(2)

@when (u'user click to the received message')
def step_impl(context):
    #time.sleep(5)
    submitButton = context.browser.find_element_by_xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div[8]/div/div[1]/div[3]/div/table/tbody/tr[1]")
    assert submitButton
    submitButton.click()
    time.sleep(4)
    #if(WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//img[@class="ajT"][last()]')))):
    submitButton = context.browser.find_element_by_xpath('//img[@class="ajT"][last()]')
    assert submitButton
    submitButton.click()
    time.sleep(3)

@when(u'user click to accept button on the gmail')
def step_impl(context):
    time.sleep(2)
    submitButton = context.browser.find_element_by_xpath("//*[text()='Accept']")
    assert submitButton
    submitButton.click()
    #time.sleep(5)

@then(u'verify displaying the subscribe page')
def step_impl(context):
    #WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.ID, "inputFirstName")))
    #time.sleep(5)
    #assert context.browser.find_element_by_xpath("//*[text() = 'Group Subscribe']")
    assert context.browser.find_element_by_id('inputFirstName')

@when(u'user click to confirm link')
def step_impl(context):
    time.sleep(2)
    submitButton = context.browser.find_element_by_xpath('//*[text()="Verify"]')
    assert submitButton
    submitButton.click()