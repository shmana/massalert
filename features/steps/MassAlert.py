import random
import time
#import pyautogui
#from features import environment
from behave import *
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By


list_low = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
list_high = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
list_digital = ["1","2","3","4","5","6","7","8","9","0"]
list_special = ["«","»","%","&","!","*","-","_",".","=","@","[","]","'","{","}","#","$",'"']
list_latin = ["À","Â","Ä","È","É","Ê","Ë","Î","Ï","Ô","Ù","Û","Ü","à","â","ä","è","é","ê","ë","î","ï","ô","ù","û","ü","ÿ","Ç","ç","Ä","ä","Ö","ö","Ü","ü","ß","æ","Æ"]
new_low_count = random.choice(list_low)
new_high_count = random.choice(list_high)
new_digital_count = random.choice(list_digital)
new_special_count = random.choice(list_special)
new_latin_count = random.choice(list_latin)
new_count = random.uniform(0, 5)
outline = random.randint(100000, 999999)
new_count_name = random.randint(100, 999)



error_name = '"Name"'
error_description = '"Description"'
error_on_clear = '"On Clear'

@given("displaying the automated user")
def load_login_page(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.ID, 'left-bar')))
    assert context.browser.find_element_by_id("left-bar")

@when('user click to MassAlerts icon')
def user_click_login_button(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'i[title="MassAlerts"]')))
    submitButton = context.browser.find_element_by_css_selector('i[title="MassAlerts"]')
    assert submitButton
    submitButton.click()
@when('user click to create MassAlert icon')
def user_click_login_button(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'img[src="static/img/btns/add.svg"]')))
    submitButton = context.browser.find_element_by_css_selector('img[src="static/img/btns/add.svg"]')
    assert submitButton
    submitButton.click()
    #time.sleep(2)

@when('user enter the MassAlert Name')

def step_impl(context):
    #time.sleep(1)
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.ID, 'mass-alert-name')))
    context.browser.find_element_by_id("mass-alert-name").clear()
    assert context.browser.find_element_by_id("mass-alert-name")
    context.browser.find_element_by_id("mass-alert-name").send_keys("Auto Test Name " + str(new_count))

@when('user enter the MassAlert Description')
def step_impl(context):
    #time.sleep(5)
    context.browser.find_element_by_id("mass-alert-desc").clear()
    assert context.browser.find_element_by_id("mass-alert-desc")
    context.browser.find_element_by_id("mass-alert-desc").send_keys("Auto Test Description " + str(new_count))

@when('user enter the MassAlert Pin code')
def step_impl(context):
    #time.sleep(1)
        context.browser.find_element_by_id("code-desc").clear()
        assert context.browser.find_element_by_id("code-desc")
        context.browser.find_element_by_id("code-desc").send_keys("1111")


@when(u'user click to Internal Tab')
def step_impl(context):
    time.sleep(1)
    submitButton = context.browser.find_element_by_xpath('//*[text()="Internal"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'div[role="tablist"]')))


@when(u'user click to the Paging group dropdown list on the On Start block')
def step_impl(context):
    time.sleep(1)
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@class="vs__search"][1]')))

    submitButton = context.browser.find_element_by_xpath('(//div[@class="w-50 mr-sm"]//div[@class="vs__dropdown-toggle"])[1]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'ul[class="vs__dropdown-menu"]')))

@when(u'user select the Group for Auto Tests on the On Start block')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'Group for Auto Test')][1]")
    assert submitButton
    submitButton.click()
@when(u'user select the Group for Auto Tests 1 On the Clear Block')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'Group for Auto Tests 2')]")
    assert submitButton
    submitButton.click()

@when(u'user enter the Message to the Message input on the On Start block')
def step_impl(context):

    context.browser.find_element_by_xpath('//textarea[@placeholder="Enter Message (Max 1024 characters)"][1]').clear()
    assert context.browser.find_element_by_xpath('//textarea[@placeholder="Enter Message (Max 1024 characters)"][1]')
    context.browser.find_element_by_xpath('//textarea[@placeholder="Enter Message (Max 1024 characters)"][1]').send_keys("Auto Test Message On Start " + str(new_count))
#######
@when(u'user click to the Paging group dropdown list on the On the Clear Block')
def step_impl(context):
    #time.sleep(2)
    submitButton = context.browser.find_element_by_xpath('(//input[@class="vs__search"])[4]')
    assert submitButton
    submitButton.click()

@when(u'user select the Group for Auto Tests On the Clear Block')
def step_impl(context):
    #time.sleep(2)
    submitButton = context.browser.find_element_by_xpath("(//*[contains(text(), 'Group for Auto Test')])[3]")
    assert submitButton
    submitButton.click()
@when(u'user enter the Message to the Message input On the Clear Block')
def step_impl(context):
    #time.sleep(2)
    context.browser.find_element_by_xpath('(//textarea[@placeholder="Enter Message (Max 1024 characters)"])[2]').clear()
    assert context.browser.find_element_by_xpath('(//textarea[@placeholder="Enter Message (Max 1024 characters)"])[2]')
    context.browser.find_element_by_xpath('(//textarea[@placeholder="Enter Message (Max 1024 characters)"])[2]').send_keys(
        "Auto Test Message On Clear " + str(new_count))
    #time.sleep(4)

@when(u'user click to API\'s Tab')
def step_impl(context):

    submitButton = context.browser.find_element_by_xpath('//*[text()="API\'s"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//button[contains(text(), 'Create')]")))
    #time.sleep(1000)
@when(u'user click to create button')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//button[contains(text(), 'Create')]")
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//*[text()="START"]')))


@then(u'verify displaying the created MassAlert')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//*[text()='Auto Test Name " + str(new_count) + "']")))
    assert context.browser.find_element_by_xpath("//*[text()='Auto Test Name " + str(new_count) + "']")

@when(u'user click to selected MassAlert')
def step_impl(context):
    #time.sleep(1000)
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'div[class="d-flex flex-column mt-md"]')))
    #submitButton = context.browser.find_element_by_xpath("(//*[text()='Auto Test Name " + str(new_count) + "'])[1]")
    submitButton = context.browser.find_element_by_xpath("(//*[contains(text(), 'Auto Test Name " + str(new_count) + "')])[1]")
    assert submitButton
    submitButton.click()
    time.sleep(2)

@when(u'user click to start MassAlert')
def step_impl(context):
    time.sleep(1)
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//*[text()="START"]')))

    submitButton = context.browser.find_element_by_xpath('//*[text()="START"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id="pass"]')))


@when(u'user enter the Pin code MassAlert')
def step_impl(context):

    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.ID, "pinCodeModal___BV_modal_body_")))
    context.browser.find_element_by_id("pass").clear()
    assert context.browser.find_element_by_id("pass")
    context.browser.find_element_by_id("pass").send_keys("1111")
   # time.sleep(5)
@when(u'user click to the Start button')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), '  Start')]")
    assert submitButton
    submitButton.click()


@then(u'verify displaying pop up window about successful started MassAlert')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'MassAlert started successfully')]")))
    assert context.browser.find_element_by_xpath("//*[contains(text(), 'MassAlert started successfully')]")


@when(u'user click to the Close button')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'Close')]")
    assert submitButton
    submitButton.click()

@then(u'verify MassAlert has started')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'STOP')]")))
    assert context.browser.find_element_by_xpath("//*[contains(text(), 'STOP')]")

@when(u'user click to stop MassAlert')
def step_impl(context):

    #WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//*[text()="STOP"]')))
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'STOP')]")
    assert submitButton
    submitButton.click()
    time.sleep(2)
@when(u'user click to the Stop button')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'Stop')]")
    assert submitButton
    submitButton.click()

@when(u'user click to close button on the Accepted pop up')
def step_impl(context):
    submitButton = context.browser.find_element_by_css_selector('button[class="close ml-auto mb-1"]')
    assert submitButton
    submitButton.click()

@then(u'verify MassAlert has finished')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'START')]")))
    assert context.browser.find_element_by_xpath("//*[contains(text(), 'START')]")


@when(u'user click to the all tab')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'All')]")
    assert submitButton
    submitButton.click()

@when(u'user click to the search icon on the MassAlert page')
def step_impl(context):
    submitButton = context.browser.find_element_by_css_selector('img[src="static/img/btns/search.svg"]')
    assert submitButton
    submitButton.click()


@then(u'verify displaying the MassAlert')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[text()='Auto Test Name " + str(new_count) + "']")))
    assert context.browser.find_element_by_xpath("//*[text()='Auto Test Name " + str(new_count) + "']")


@when(u'user click to the my tab')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'My')]")
    assert submitButton
    submitButton.click()
@when(u'user enter the MassAlert Name to the search input')
def step_impl(context):
    context.browser.find_element_by_css_selector('input[placeholder="Search..."]').clear()
    assert context.browser.find_element_by_css_selector('input[placeholder="Search..."]')
    context.browser.find_element_by_css_selector('input[placeholder="Search..."]').send_keys('Auto Test Name ' + str(new_count) + '')
    #time.sleep(5)

@when(u'user click to the Delete button')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'Delete')]")
    assert submitButton
    submitButton.click()

@when(u'user confirm delete MassAlert')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'button[class="btn q-btn-default mr-sm"]')))
    submitButton = context.browser.find_element_by_css_selector('button[class="btn q-btn-default mr-sm"]')
    assert submitButton
    submitButton.click()

@then(u'verify not displaying MassAlert')
def step_impl(context):
    context.browser.find_element_by_css_selector('img[src="static/img/btns/search.svg"]').click()

    context.browser.find_element_by_css_selector('input[placeholder="Search..."]').clear()
    assert context.browser.find_element_by_css_selector('input[placeholder="Search..."]')
    context.browser.find_element_by_css_selector('input[placeholder="Search..."]').send_keys('Auto Test Name ' + str(new_count) + '')
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'div[class="not-found text-center text-gray-md"]')))
    assert context.browser.find_element_by_css_selector('div[class="not-found text-center text-gray-md"]')
    #time.sleep(5)

@when(u'User click to the edit button')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'Edit')]")
    assert submitButton
    submitButton.click()

@when(u'user enter the new MassAlert Name')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, '//*[text()="Edit MassAlert"]')))
    context.browser.find_element_by_id("mass-alert-name").clear()
    assert context.browser.find_element_by_id("mass-alert-name")
    context.browser.find_element_by_id("mass-alert-name").send_keys("NEW Auto Test Name " + str(new_count))

@when(u'user enter the new MassAlert Description')
def step_impl(context):
    context.browser.find_element_by_id("mass-alert-desc").clear()
    assert context.browser.find_element_by_id("mass-alert-desc")
    context.browser.find_element_by_id("mass-alert-desc").send_keys("NEW Auto Test Description " + str(new_count))


@when(u'user click to save button')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'Save')]")
    assert submitButton
    submitButton.click()

@then(u'verify displaying new MassAlert name')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[text()='NEW Auto Test Name " + str(new_count) + "']")))
    assert context.browser.find_element_by_xpath("//*[text()='NEW Auto Test Name " + str(new_count) + "']")

@then(u'verify displaying new MassAlert Description')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[text()='NEW Auto Test Description " + str(new_count) + "']")))
    assert context.browser.find_element_by_xpath("//*[contains(text(), 'NEW Auto Test Description " + str(new_count) + "')]")
    #time.sleep(5)

@when(u'user click to Feed icon')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//div[@class="d-flex justify-content-center"][1]')
    assert submitButton
    submitButton.click()
    #WebDriverWait(context.browser, 20).until(
    #    EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'Auto Test Name " + str(new_count) + "')]")))
    #time.sleep(5)

@when(u'user click to Message about MA')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'All Quicklert')][1]")))
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'Auto Test Name " + str(new_count) + "')]")))

    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'Auto Test Name " + str(new_count) + "')]")
    submitButton.click()
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'STOP')]")))
#    time.sleep(5)

@when(u'user click to stop MassAlert on the feed page')
def step_impl(context):
    time.sleep(1)

    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'STOP')]")))
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located(
            (By.XPATH, "//*[contains(text(), 'Auto Test Message On Start " + str(new_count) + "')]")))
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'STOP')]")
    assert submitButton
    submitButton.click()
#    time.sleep(5)

@then(u'verify on feed page that MassAlert has finished')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//div[@class="col p-0 h-full overflow-hidden"]//img[@src="static/img/default_mass_alert.svg"]')))
    assert context.browser.find_element_by_xpath('//div[@class="col p-0 h-full overflow-hidden"]//img[@src="static/img/default_mass_alert.svg"]')

@when(u'user click to the Summery button')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'Summary')]")
    assert submitButton
    submitButton.click()

@then(u'verify displaying the doughnut chart')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.ID, "doughnut-chart")))
    assert context.browser.find_element_by_id('doughnut-chart')

@then(u'verify displaying the chart size monitor')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.ID, "line-chart")))
    assert context.browser.find_element_by_id('line-chart')

@then(u'verify displaying MassAlert title')
def step_impl(context):
    time.sleep(1)
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'MassAlerts')]")))
    assert context.browser.find_element_by_xpath("//*[contains(text(), 'MassAlerts')]")

@then(u'verify displaying the create MassAlert icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, "div[title= 'Create MassAlert']")))
    assert context.browser.find_element_by_css_selector("div[title= 'Create MassAlert']")

@then(u'verify displaying the refresh list icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'div[title="Refresh list"]')))
    assert context.browser.find_element_by_css_selector('div[title="Refresh list"]')

@then(u'verify displaying the search icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'div[class="input-group search"]')))
    assert context.browser.find_element_by_css_selector('div[class="input-group search"]')

@then(u'verify displaying the All block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'All')]")))
    assert context.browser.find_element_by_xpath("//*[contains(text(), 'All')]")

@then(u'verify displaying the My block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'My')]")))
    assert context.browser.find_element_by_xpath("//*[contains(text(), 'My')]")

@then(u'verify displaying the Shared block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'Shared')]")))
    assert context.browser.find_element_by_xpath("//*[contains(text(), 'Shared')]")

@then(u'verify displaying the "Please select MassAlert" text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'Please select MassAlert')]")))
    assert context.browser.find_element_by_xpath("//*[contains(text(), 'Please select MassAlert')]")

@then(u'verify displaying the Icons and titles on MassAlerts')
def step_impl(context):
    #icon
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'div[class^="mass-alert-item d-flex flex-column align-items-center item mb-md"]')))
    assert context.browser.find_elements_by_css_selector('div[class^="mass-alert-item d-flex flex-column align-items-center item mb-md"]')
    #title
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'div[class^="mass-alert-item-name text-primary break-long-word"]')))
    assert context.browser.find_elements_by_css_selector('div[class^="mass-alert-item-name text-primary break-long-word"]')

    icons = len(context.browser.find_elements_by_css_selector('div[class^="mass-alert-item d-flex flex-column align-items-center item mb-md"]'))
    titles = len(context.browser.find_elements_by_css_selector('div[class^="mass-alert-item-name text-primary break-long-word"]'))
    assert icons == titles

@then(u'verify displaying the "Create MassAlert" text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
            (By.XPATH, "//*[text()='Create MassAlert']")))
    assert context.browser.find_element_by_xpath("//*[text()='Create MassAlert']")


@then(u'verify displaying the Information tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
            (By.XPATH, "//*[text()='Information']")))
    assert context.browser.find_element_by_xpath("//*[text()='Information']")

@then(u'verify displaying the Internal tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
            (By.XPATH, "//*[text()='Internal']")))
    assert context.browser.find_element_by_xpath("//*[text()='Internal']")

@then(u'verify displaying the External tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
            (By.XPATH, "//*[text()='External']")))
    assert context.browser.find_element_by_xpath("//*[text()='External']")

@then(u'verify displaying the QBOX tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
            (By.XPATH, "//*[text()='QBOX']")))
    assert context.browser.find_element_by_xpath("//*[text()='QBOX']")

@then(u'verify displaying the Location tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
            (By.XPATH, "//*[text()='Location']")))
    assert context.browser.find_element_by_xpath("//*[text()='Location']")

@then(u'verify displaying the API\'s tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
            (By.XPATH, "//*[contains(text(), 'API')]")))
    assert context.browser.find_element_by_xpath("//*[contains(text(), 'API')]")

@then(u'verify displaying the Next button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
            (By.XPATH, "//button[contains(text(),'Next')]")))
    assert context.browser.find_element_by_xpath("//button[contains(text(),'Next')]")

@then(u'verify displaying the Photo block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
            (By.CSS_SELECTOR, 'div[class="mass-alert-item-avatar"]')))
    assert context.browser.find_element_by_css_selector('div[class="mass-alert-item-avatar"]')

@then(u'verify displaying the Change icon link')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
            (By.XPATH, "//*[text()='Change icon']")))
    assert context.browser.find_element_by_xpath("//*[text()='Change icon']")

@then(u'verify displaying the Name text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
            (By.XPATH, "//*[text()='Name *']")))
    assert context.browser.find_element_by_xpath("//*[text()='Name *']")

@then(u'verify displaying the Name input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
            (By.CSS_SELECTOR, 'input[placeholder="Enter MassAlert Name"]')))
    assert context.browser.find_element_by_css_selector('input[placeholder="Enter MassAlert Name"]')

@then(u'verify displaying the Description text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
            (By.XPATH, "//*[text()='Description *']")))
    assert context.browser.find_element_by_xpath("//*[text()='Description *']")

@then(u'verify displaying the Description input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
            (By.CSS_SELECTOR, 'input[placeholder="Enter MassAlert Description"]')))
    assert context.browser.find_element_by_css_selector('input[placeholder="Enter MassAlert Description"]')

@then(u'verify displaying the Pincode text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "//*[text()='Description *']")))
    assert context.browser.find_element_by_xpath("//*[text()='Pincode *']")


@then(u'verify displaying the Pincode input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.CSS_SELECTOR, 'input[placeholder="Enter Code (4-10 numbers)"]')))
    assert context.browser.find_element_by_css_selector('input[placeholder="Enter Code (4-10 numbers)"]')

@then(u'verify displaying the Allow Reply text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//*[text()="Allow Reply"]')))
    assert context.browser.find_element_by_xpath('//*[text()="Allow Reply"]')

@then(u'verify displaying the Allow Reply checkbox')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//input[@name="allow-reply-mass-alert-checkbox"]')))
    assert context.browser.find_element_by_xpath('//input[@name="allow-reply-mass-alert-checkbox"]')

@then(u'verify displaying the Repeat MassAlert text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//*[text()="Repeat MassAlert"]')))
    assert context.browser.find_element_by_xpath('//*[text()="Repeat MassAlert"]')

@then(u'verify displaying the Repeat MassAlert checkbox')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//input[@name="repeat-mass-alert-checkbox"]')))
    assert context.browser.find_element_by_xpath('//input[@name="repeat-mass-alert-checkbox"]')

@then(u'verify displaying the Allow Acknowledge text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//*[text()="Allow Acknowledge"]')))
    assert context.browser.find_element_by_xpath('//*[text()="Allow Acknowledge"]')

@then(u'verify displaying the Allow Acknowledge checkbox')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//input[@name="allow-ack-mass-alert-checkbox"]')))
    assert context.browser.find_element_by_xpath('//input[@name="allow-ack-mass-alert-checkbox"]')

@then(u'verify displaying the Priority Profile text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//*[text()="Priority Profile"]')))
    assert context.browser.find_element_by_xpath('//*[text()="Priority Profile"]')

@then(u'verify displaying the Priority Profile dropdown list')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="vs__dropdown-toggle"]')))
    assert context.browser.find_element_by_xpath('//div[@class="vs__dropdown-toggle"]')

@when(u'user click to Priority Profile dropdown list')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//div[@class="vs__dropdown-toggle"]')
    assert submitButton
    submitButton.click()
    # WebDriverWait(context.browser, 20).until(
    #    EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'Auto Test Name " + str(new_count) + "')]")))
    #time.sleep(5)


@then(u'verify displaying the Critical option')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//li[contains(.,"Critical")]')))
    assert context.browser.find_element_by_xpath('//li[contains(.,"Critical")]')

@then(u'verify displaying the Warning option')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//li[contains(.,"Warning")]')))
    assert context.browser.find_element_by_xpath('//li[contains(.,"Warning")]')

@then(u'verify displaying the Normal option')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//li[contains(.,"Normal")]')))
    assert context.browser.find_element_by_xpath('//li[contains(.,"Normal")]')

@when(u'user click to the Share With tab')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'Shared With')]")
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//*[contains(text(), "list is empty")]')))

@when(u'user click to the Share icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@title="Share"]')))
    submitButton = context.browser.find_element_by_xpath('//div[@title="Share"]')
    assert submitButton
    submitButton.click()

@when(u'user enter the user name')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.ID, 'share-with-input')))
    context.browser.find_element_by_id("share-with-input").clear()
    assert context.browser.find_element_by_id("share-with-input")
    context.browser.find_element_by_id("share-with-input").send_keys("massalert1 massalert1")

@when(u'user click to the search icon')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//img[@class="img-15"]')
    assert submitButton
    submitButton.click()

@when(u'user click to the execute checkbox')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "//*[text() = 'Execute']")))
    submitButton = context.browser.find_element_by_xpath("//*[text() = 'Execute']")
    assert submitButton
    submitButton.click()

@then(u'verify status checkboxes for execute permissions')
def step_impl(context):
    assert context.browser.find_element_by_xpath('(//input[@type="checkbox"])[2]').is_selected() == True
    assert context.browser.find_element_by_xpath('(//input[@type="checkbox"])[1]').is_selected() == False

@when(u'user click to the share button')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//button[contains(text(), 'Share')]")
    assert submitButton
    submitButton.click()

@then(u'verify displaying the shared user')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//*[text()="massalert1 massalert1"]')))
    assert context.browser.find_element_by_xpath('//*[text()="massalert1 massalert1"]')

@when(u'user click to the accepted message')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'All Quicklert')][1]")))
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'Auto Test Name "+ str(new_count) +"')]")))
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'Auto Test Name "+ str(new_count) +"')]")
    assert submitButton
    submitButton.click()


@when(u'user click to the Accept button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//button[contains(text(), 'Accept')]")))
    submitButton = context.browser.find_element_by_xpath("//button[contains(text(), 'Accept')]")
    assert submitButton
    submitButton.click()

@then(u'verify displaying the Accepted pop up')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="b-toast b-toast-append b-toast-success"]')))
    assert context.browser.find_element_by_xpath('//div[@class="b-toast b-toast-append b-toast-success"]')

@then(u'verify displaying the Details tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//*[text()= "Details"]')))
    assert context.browser.find_element_by_xpath('//*[text()= "Details"]')

@then(u'verify displaying the History tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//*[text()= "History"]')))
    assert context.browser.find_element_by_xpath('//*[text()= "History"]')

@then(u'verify displaying the Shared With tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//*[text()= "Shared With"]')))
    assert context.browser.find_element_by_xpath('//*[text()= "Shared With"]')

@then(u'verify displaying the Subscribers tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//*[text()= "Subscribers"]')))
    assert context.browser.find_element_by_xpath('//*[text()= "Subscribers"]')

@then(u'verify displaying the Start button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[text()= "START"]')))
    assert context.browser.find_element_by_xpath('//div[text()= "START"]')

@then(u'verify not displaying the Edit button')
def step_impl(context):
    text1 = context.browser.find_elements_by_css_selector('button[class^="btn"]')
    list1 = []
    for i in text1:
        text = i.text
        list1.append(text)
    my_list = []
    result = list(set(my_list) - set(list1))
    print(result)
    assert "Edit" not in result

@then(u'verify not displaying the Delete button')
def step_impl(context):
    text1 = context.browser.find_elements_by_css_selector('button[class^="btn"]')
    list1 = []
    for i in text1:
        text = i.text
        list1.append(text)
    my_list = []
    result = list(set(my_list) - set(list1))
    print(result)
    assert "Delete" not in result

@when(u'user click to the Shared tab')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//*[text()="Shared"]')
    assert submitButton
    submitButton.click()
    #time.sleep(2)

@when(u'user click to the edit checkbox')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "//*[text() = 'Edit']")))
    submitButton = context.browser.find_element_by_xpath("//*[text() = 'Edit']")
    assert submitButton
    submitButton.click()

@then(u'verify status checkboxes for edit permissions')
def step_impl(context):
    assert context.browser.find_element_by_xpath('(//input[@type="checkbox"])[2]').is_selected() == True
    assert context.browser.find_element_by_xpath('(//input[@type="checkbox"])[1]').is_selected() == True

@then(u'verify displaying the Edit button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "//button[contains(text(), 'Edit')]")))
    assert context.browser.find_element_by_xpath("//button[contains(text(), 'Edit')]")

@then(u'verify displaying the Paging Group Notification block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="card mb-xs bg-transparent border border-gray rounded-3"][1]')))
    assert context.browser.find_element_by_xpath('//div[@class="card mb-xs bg-transparent border border-gray rounded-3"][1]')
    assert context.browser.find_element_by_xpath('//div[@aria-controls="accordion-1"]')
    assert context.browser.find_element_by_xpath('//div[@id="accordion-1"]')

@then(u'verify displaying the On Start text on the Paging Group Notification block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//*[@id="accordion-1"]//*[text()="On Start"]')))
    assert context.browser.find_element_by_xpath('//*[@id="accordion-1"]//*[text()="On Start"]')

@then(u'verify displaying the Paging group text for On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="w-50 mr-sm"]//*[text()="Paging Group"]')))
    assert context.browser.find_element_by_xpath('//div[@class="w-50 mr-sm"]//*[text()="Paging Group"]')

@then(u'verify displaying the Paging group dropdown list for On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="w-50 mr-sm"]//div[@id="vs2__combobox"]')))
    assert context.browser.find_element_by_xpath('//div[@class="w-50 mr-sm"]//div[@id="vs2__combobox"]')

@then(u'verify displaying the My Group text for On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="w-50 mr-sm"]//*[text()="My Group"]')))
    assert context.browser.find_element_by_xpath('//div[@class="w-50 mr-sm"]//*[text()="My Group"]')

@then(u'verify displaying the My Group dropdown list for On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="w-50 mr-sm"]//div[@id="vs3__combobox"]')))
    assert context.browser.find_element_by_xpath('//div[@class="w-50 mr-sm"]//div[@id="vs3__combobox"]')
@then(u'verify displaying the On Clear text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="w-50 ml-sm"]//*[text()="On Clear"]')))
    assert context.browser.find_element_by_xpath('//div[@class="w-50 ml-sm"]//*[text()="On Clear"]')

@then(u'verify displaying the Paging group text for On Clear block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="w-50 ml-sm"]//*[text()="Paging Group"]')))
    assert context.browser.find_element_by_xpath('//div[@class="w-50 ml-sm"]//*[text()="Paging Group"]')

@then(u'verify displaying the Paging group dropdown list for On Clear block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="w-50 ml-sm"]//*[@id="vs4__combobox"]')))
    assert context.browser.find_element_by_xpath('//div[@class="w-50 ml-sm"]//*[@id="vs4__combobox"]')

@then(u'verify displaying the My Group text for On Clear block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="w-50 ml-sm"]//*[text()="My Group"]')))
    assert context.browser.find_element_by_xpath('//div[@class="w-50 ml-sm"]//*[text()="My Group"]')

@then(u'verify displaying the My Group dropdown list for On Clear block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="w-50 ml-sm"]//*[@id="vs5__combobox"]')))
    assert context.browser.find_element_by_xpath('//div[@class="w-50 ml-sm"]//*[@id="vs5__combobox"]')

@when(u'user click to Conference Group block')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//div[@aria-controls="accordion-2"]')
    assert submitButton
    submitButton.click()

@then(u'verify displaying the Conference Group block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '(//div[@class="card mb-xs bg-transparent border border-gray rounded-3"])[2]')))
    assert context.browser.find_element_by_xpath('(//div[@class="card mb-xs bg-transparent border border-gray rounded-3"])[2]')
    assert context.browser.find_element_by_xpath('//div[@aria-controls="accordion-2"]')
    assert context.browser.find_element_by_xpath('//div[@id="accordion-2"]')

@then(u'verify displaying the Conference Group dropdown list')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,'//input[@placeholder="Select Conference Group"]')))
    assert context.browser.find_element_by_xpath('//input[@placeholder="Select Conference Group"]')


@when(u'user click to the Conference Group dropdown list')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="form-group mb-0"]//input[@placeholder="Select Conference Group"]')))
    submitButton = context.browser.find_element_by_xpath('//div[@class="form-group mb-0"]//input[@placeholder="Select Conference Group"]')
    assert submitButton
    submitButton.click()

@then(u'verify displaying the no matching option')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "//*[text()='Sorry, no matching options.']")))
    assert context.browser.find_element_by_xpath("//*[text()='Sorry, no matching options.']")
    #time.sleep(5)

@when(u'user click to the DialOut Group Notification block')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//div[@aria-controls="accordion-3"]')
    assert submitButton
    submitButton.click()

@then(u'verify displaying the DialOut Group Notification block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '(//div[@class="card mb-xs bg-transparent border border-gray rounded-3"])[3]')))
    assert context.browser.find_element_by_xpath('(//div[@class="card mb-xs bg-transparent border border-gray rounded-3"])[3]')
    assert context.browser.find_element_by_xpath('//div[@aria-controls="accordion-3"]')
    assert context.browser.find_element_by_xpath('//div[@id="accordion-3"]')

@then(u'verify displaying the On Start text on the DialOut Group Notification block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '(//div[@class="card mb-xs bg-transparent border border-gray rounded-3"])[3]//*[text()="On Start"]')))
    assert context.browser.find_element_by_xpath('(//div[@class="card mb-xs bg-transparent border border-gray rounded-3"])[3]//*[text()="On Start"]')

@then(u'user verify the DialOut Group Notification dropdown list')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//input[@placeholder="Select Dial Out Group"]')))
    assert context.browser.find_element_by_xpath('//input[@placeholder="Select Dial Out Group"]')

@when(u'user click to On Start dropdown list')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//div[@class="v-select vs--single vs--searchable"]//input[@placeholder="Select Dial Out Group"]')
    assert submitButton
    submitButton.click()
    #time.sleep(5)

@when(u'user click to Paging Group Notification block')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//div[@aria-controls="accordion-1"]')
    assert submitButton
    submitButton.click()

@then(u'verify displaying Message Type text on the On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="w-50 mr-sm"]//*[text()="Message Type"]')))
    assert context.browser.find_element_by_xpath('//div[@class="w-50 mr-sm"]//*[text()="Message Type"]')

@then(u'verify displaying the Message Type dropdown list on the On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="w-50 mr-sm"]//*[@id="vs7__combobox"]')))
    assert context.browser.find_element_by_xpath('//div[@class="w-50 mr-sm"]//*[@id="vs7__combobox"]')

@when(u'user enter the MassAlert {name} Name Outline')
def step_impl(context, name):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, '//*[text()="Create MassAlert"]')))
    #time.sleep(7)
    if name == "spaces":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-name"]')))
        context.browser.find_element_by_id("mass-alert-name").clear()
        assert context.browser.find_element_by_id("mass-alert-name")
        context.browser.find_element_by_id("mass-alert-name").send_keys('  ')
        #time.sleep(3)
    elif name == "empty":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-name"]')))
        context.browser.find_element_by_id("mass-alert-name").clear()
        assert context.browser.find_element_by_id("mass-alert-name")
        #time.sleep(3)
    elif name == "one_symbol_low":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-name"]')))
        context.browser.find_element_by_id("mass-alert-name").clear()
        assert context.browser.find_element_by_id("mass-alert-name")
        context.browser.find_element_by_id("mass-alert-name").send_keys(new_low_count)
    elif name == "one_symbol_high":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-name"]')))
        context.browser.find_element_by_id("mass-alert-name").clear()
        assert context.browser.find_element_by_id("mass-alert-name")
        context.browser.find_element_by_id("mass-alert-name").send_keys(new_high_count)
    elif name == "one_symbol_digital":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-name"]')))
        context.browser.find_element_by_id("mass-alert-name").clear()
        assert context.browser.find_element_by_id("mass-alert-name")
        context.browser.find_element_by_id("mass-alert-name").send_keys(new_digital_count)
    elif name == "one_symbol_special":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-name"]')))
        context.browser.find_element_by_id("mass-alert-name").clear()
        assert context.browser.find_element_by_id("mass-alert-name")
        context.browser.find_element_by_id("mass-alert-name").send_keys(new_special_count)
    elif name == "one_symbol_latin":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-name"]')))
        context.browser.find_element_by_id("mass-alert-name").clear()
        assert context.browser.find_element_by_id("mass-alert-name")
        context.browser.find_element_by_id("mass-alert-name").send_keys(new_latin_count)
    else:
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-name"]')))
        context.browser.find_element_by_id("mass-alert-name").clear()
        assert context.browser.find_element_by_id("mass-alert-name")
        context.browser.find_element_by_id("mass-alert-name").send_keys(str(outline) + name)
        #time.sleep(7)


@then(u'verify displaying the error pop up for name input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,  "//*[contains(text(), '"+ str(error_name)+" field is mandatory and should contain up to 50 characters. Please check Information Tab')]")))
    assert context.browser.find_element_by_xpath("//*[contains(text(), '"+ str(error_name)+" field is mandatory and should contain up to 50 characters. Please check Information Tab')]")

@then(u'verify displaying the error pop up for description input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,  "//*[contains(text(), '"+ str(error_description)+" field is mandatory and should contain up to 250 characters. Please check Information Tab')]")))
    assert context.browser.find_element_by_xpath("//*[contains(text(), '"+ str(error_description)+" field is mandatory and should contain up to 250 characters. Please check Information Tab')]")



@when(u'user click to back button')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//img[@src="static/img/back.svg"]')
    assert submitButton
    submitButton.click()


@when(u'user confirm leave')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "//button[text()='Discard']")))
    submitButton = context.browser.find_element_by_xpath("//button[text()='Discard']")
    assert submitButton
    submitButton.click()
@when(u'user click to create button for outline')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//button[contains(text(), 'Create')]")
    assert submitButton
    submitButton.click()
    time.sleep(2)
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//div[text()= 'START']")))

@when(u'user click to create button for incorrect outline')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//button[contains(text(), 'Create')]")
    assert submitButton
    submitButton.click()
    #time.sleep(2)
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, '//div[@class="b-toast b-toast-append b-toast-danger"]')))



@when(u'user enter the MassAlert {description} Description Outline')
def step_impl(context, description):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, '//*[text()="Create MassAlert"]')))
    #time.sleep(7)
    if description == "spaces":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-desc"]')))
        context.browser.find_element_by_id("mass-alert-desc").clear()
        assert context.browser.find_element_by_id("mass-alert-desc")
        context.browser.find_element_by_id("mass-alert-desc").send_keys('  ')
        #time.sleep(3)
    elif description == "empty":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-desc"]')))
        context.browser.find_element_by_id("mass-alert-desc").clear()
        assert context.browser.find_element_by_id("mass-alert-desc")
        #time.sleep(3)
    elif description == "one_symbol_low":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-desc"]')))
        context.browser.find_element_by_id("mass-alert-desc").clear()
        assert context.browser.find_element_by_id("mass-alert-desc")
        context.browser.find_element_by_id("mass-alert-desc").send_keys(new_low_count)
    elif description == "one_symbol_high":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-desc"]')))
        context.browser.find_element_by_id("mass-alert-desc").clear()
        assert context.browser.find_element_by_id("mass-alert-desc")
        context.browser.find_element_by_id("mass-alert-desc").send_keys(new_high_count)
    elif description == "one_symbol_digital":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-desc"]')))
        context.browser.find_element_by_id("mass-alert-desc").clear()
        assert context.browser.find_element_by_id("mass-alert-desc")
        context.browser.find_element_by_id("mass-alert-desc").send_keys(new_digital_count)
    elif description == "one_symbol_special":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-desc"]')))
        context.browser.find_element_by_id("mass-alert-desc").clear()
        assert context.browser.find_element_by_id("mass-alert-desc")
        context.browser.find_element_by_id("mass-alert-desc").send_keys(new_special_count)
    elif description == "one_symbol_latin":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-desc"]')))
        context.browser.find_element_by_id("mass-alert-desc").clear()
        assert context.browser.find_element_by_id("mass-alert-desc")
        context.browser.find_element_by_id("mass-alert-desc").send_keys(new_latin_count)
    else:
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//input[@id ="mass-alert-desc"]')))
        context.browser.find_element_by_id("mass-alert-desc").clear()
        assert context.browser.find_element_by_id("mass-alert-desc")
        context.browser.find_element_by_id("mass-alert-desc").send_keys(str(outline) + description)
        #time.sleep(3)

@when(u'user enter the Message to the Message {message} input On the Clear Block On the Internal Tab')
def step_impl(context, message):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, '//*[text()="Create MassAlert"]')))
    #time.sleep(7)
    if message == "spaces":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys('  ')
        #time.sleep(3)
    elif message == "empty":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        #context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").clear()
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(Keys.CONTROL + "a")
        #time.sleep(5)
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(Keys.DELETE)
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        #time.sleep(5)

    elif message == "one_symbol_low":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(new_low_count)
    elif message == "one_symbol_high":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(new_high_count)
    elif message == "one_symbol_digital":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(new_digital_count)
    elif message == "one_symbol_special":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(new_special_count)
    elif message == "one_symbol_latin":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(new_latin_count)
    else:
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(str(outline) + message)
        #time.sleep(3)

@then(u'verify displaying the error pop up for message')
def step_impl(context):
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
            (By.XPATH,'//div[@class="b-toast b-toast-append b-toast-danger"]')))
        assert context.browser.find_element_by_xpath('//div[@class="b-toast b-toast-append b-toast-danger"]')

@when(u'user enter the Message to the Message {message} input On the Start Block On the Internal Tab')
def step_impl(context, message):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, '//*[text()="Create MassAlert"]')))
    #time.sleep(7)
    if message == "spaces":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys('  ')
        #time.sleep(3)
    elif message == "empty":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(Keys.CONTROL + "a")
        #time.sleep(5)
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(Keys.DELETE)
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        #time.sleep(5)
    elif message == "one_symbol_low":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(new_low_count)
    elif message == "one_symbol_high":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(new_high_count)
    elif message == "one_symbol_digital":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(new_digital_count)
    elif message == "one_symbol_special":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(new_special_count)
    elif message == "one_symbol_latin":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(new_latin_count)
    else:
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Message (Max 1024 characters)']").send_keys(str(outline) + message)
        #time.sleep(3)

@when(u'user click to External Tab')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//div[text()="External"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//output[@class="sr-only"]')))
@when(u'user enter valid email data on the External tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//output[@class="sr-only"]')))
    context.browser.find_element_by_css_selector('div[class^="b-form-tags"]').click()
    context.browser.find_element_by_xpath('//input[@id="tags-separators"]').clear()
    assert context.browser.find_element_by_xpath('//input[@id="tags-separators"]')
    context.browser.find_element_by_xpath('//input[@id="tags-separators"]').send_keys("massalert.quicklert@gmail.com")
    #time.sleep(5)

@when(u'user click to the add button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.CSS_SELECTOR, 'button[class="btn b-form-tags-button py-0 btn-outline-secondary"]')))
    submitButton = context.browser.find_element_by_css_selector('button[class="btn b-form-tags-button py-0 btn-outline-secondary"]')
    assert submitButton
    submitButton.click()

@when(u'user enter the Message to the Message {message} input On the Clear Block On the External Tab')
def step_impl(context, message):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, '//*[text()="Create MassAlert"]')))
    #time.sleep(7)
    if message == "spaces":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys('  ')
        #time.sleep(3)
    elif message == "empty":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(Keys.CONTROL + "a")
        #time.sleep(5)
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(Keys.DELETE)
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        #time.sleep(3)

    elif message == "one_symbol_low":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(new_low_count)
    elif message == "one_symbol_high":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(new_high_count)
    elif message == "one_symbol_digital":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(new_digital_count)
    elif message == "one_symbol_special":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(new_special_count)
    elif message == "one_symbol_latin":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(new_latin_count)


    else:
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(str(outline) + message)
        #time.sleep(3)

@when(u'user enter the Message to the Message input On the Start block On the External Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
    context.browser.find_element_by_xpath(
        "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
    assert context.browser.find_element_by_xpath(
        "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
    context.browser.find_element_by_xpath(
        "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(
        "Auto Test Name " + str(new_count))
    #time.sleep(3)

@when(u'user enter the Message to the Message {message} input On the Start block On the External Tab')
def step_impl(context, message):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, '//*[text()="Create MassAlert"]')))
    #time.sleep(7)
    if message == "spaces":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys('  ')
        #time.sleep(3)
    elif message == "empty":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(Keys.CONTROL + "a")
        #time.sleep(5)
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(Keys.DELETE)
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        #time.sleep(3)

    elif message == "one_symbol_low":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(new_low_count)
    elif message == "one_symbol_high":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(new_high_count)
    elif message == "one_symbol_digital":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(new_digital_count)
    elif message == "one_symbol_special":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(new_special_count)
    elif message == "one_symbol_latin":
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(new_latin_count)
    else:
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
        assert context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
        context.browser.find_element_by_xpath("//div[@class='w-50 mr-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(str(outline) + message)
        #time.sleep(3)


@when(u'user enter the Message to the Message input On the Clear Block On the External Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")))
    context.browser.find_element_by_xpath(
        "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").clear()
    assert context.browser.find_element_by_xpath(
        "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']")
    context.browser.find_element_by_xpath(
        "//div[@class='w-50 ml-sm']//textarea[@placeholder = 'Enter Email Alert Message (Max 1024 characters)']").send_keys(
        "Auto Test Name " + str(new_count))
    #time.sleep(3)

@then(u'verify displaying the created MassAlert {name} by outline')
def step_impl(context, name):
        #time.sleep(5)

        if name == "one_symbol_low":
            WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
                (By.XPATH,'//div[@class="d-flex"]//div[contains(text(), '+str(new_low_count)+')][1]')))
            assert context.browser.find_element_by_xpath('//div[@class="d-flex"]//div[contains(text(), '+str(new_low_count)+')][1]')
        elif name == "one_symbol_high":
            WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
                    (By.XPATH, '//div[@class="d-flex"]//div[contains(text(), ' + str(new_high_count) + ')][1]')))
            assert context.browser.find_element_by_xpath(
                    '//div[@class="d-flex"]//div[contains(text(), ' + str(new_high_count) + ')][1]')

        elif name == "one_symbol_digital":
            WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
                    (By.XPATH, '//div[@class="d-flex"]//div[contains(text(), ' + str(new_digital_count) + ')][1]')))
            assert context.browser.find_element_by_xpath(
                    '//div[@class="d-flex"]//div[contains(text(), ' + str(new_digital_count) + ')][1]')

        elif name == "one_symbol_special":
            WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
                    (By.XPATH, '//div[@class="d-flex"]//div[text()= "'+ str(new_special_count) +'"][1]')))
            assert context.browser.find_element_by_xpath(
                    '//div[@class="d-flex"]//div[text()= "'+ str(new_special_count) +'"][1]')

        elif name == "one_symbol_latin":
            WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
                    (By.XPATH, '//div[@class="d-flex"]//div[contains(text(), ' + str(new_latin_count) + ')][1]')))
            assert context.browser.find_element_by_xpath(
                    '//div[@class="d-flex"]//div[contains(text(), ' + str(new_latin_count) + ')][1]')

@when(u'user click to the Message type dropdown list on the On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '(//div[@class="vs__dropdown-toggle"]//div[@class="vs__selected-options"])[3]')))
    submitButton = context.browser.find_element_by_xpath('(//div[@class="vs__dropdown-toggle"]//div[@class="vs__selected-options"])[3]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@dir="auto"]//ul[@class="vs__dropdown-menu"]')))
@then(u'verify displaying the Text option on the On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Text')])[1]")))
    assert context.browser.find_element_by_xpath("(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Text')])[1]")

@then(u'verify displaying the Text to Speache option on the On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Text-To-Speach')])[1]")))
    assert context.browser.find_element_by_xpath("(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Text-To-Speach')])[1]")

@then(u'verify displaying the Text and Text to Speache option on the On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Text & Text-To-Speach')])")))
    assert context.browser.find_element_by_xpath("(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Text & Text-To-Speach')])")

@then(u'verify displaying the Media file option on the On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Media File')])")))
    assert context.browser.find_element_by_xpath(
        "(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Media File')])")
    #time.sleep(5)

@when(u'user click to the Text option on the On Start block')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Text')])[1]")
    assert submitButton
    submitButton.click()
   # WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
   #     (By.XPATH, '//output[@class="sr-only"]')))

@then(u'verify displaying the Message text on the On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "//div[@class='form-group mb-0']//*[text() = 'Message']")))
    assert context.browser.find_element_by_xpath(
        "//div[@class='form-group mb-0']//*[text() = 'Message']")

@then(u'verify displaying the Message input on the On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="w-50 mr-sm"]//textarea[@placeholder="Enter Message (Max 1024 characters)"]')))
    assert context.browser.find_element_by_xpath(
        '//div[@class="w-50 mr-sm"]//textarea[@placeholder="Enter Message (Max 1024 characters)"]')

@then(u'verify displaying Message Type text On the Clear Block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="w-50 ml-sm"]//label[text()="Message Type"]')))
    assert context.browser.find_element_by_xpath(
        '//div[@class="w-50 ml-sm"]//label[text()="Message Type"]')

@then(u'verify displaying the Message Type dropdown list On the Clear Block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '(//div[@class="w-50 ml-sm"]//div[@class="vs__dropdown-toggle"])[3]')))
    assert context.browser.find_element_by_xpath(
        '(//div[@class="w-50 ml-sm"]//div[@class="vs__dropdown-toggle"])[3]')

@when(u'user click to the Message type dropdown list On the Clear Block')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('(//div[@class="vs__dropdown-toggle"]//div[@class="vs__selected-options"])[6]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@dir="auto"]//ul[@class="vs__dropdown-menu"]')))

@then(u'verify displaying the Text option On the Clear Block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Text')])[1]")))
    assert context.browser.find_element_by_xpath("(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Text')])[1]")

@then(u'verify displaying the Text to Speache option On the Clear Block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Text-To-Speach')])[1]")))
    assert context.browser.find_element_by_xpath("(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Text-To-Speach')])[1]")

@then(u'verify displaying the Text and Text to Speache option On the Clear Block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Text & Text-To-Speach')])")))
    assert context.browser.find_element_by_xpath("(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Text & Text-To-Speach')])")

@then(u'verify displaying the Media file option On the Clear Block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Media File')])")))
    assert context.browser.find_element_by_xpath(
        "(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Media File')])")
    #time.sleep(5)

@when(u'user click to the Text option On the Clear Block')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("(//ul[@class='vs__dropdown-menu']//li[contains(text(), 'Text')])[1]")
    assert submitButton
    submitButton.click()

@then(u'verify displaying the Message text On the Clear Block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="w-50 ml-sm"]//*[text()="Message"]')))
    assert context.browser.find_element_by_xpath(
        '//div[@class="w-50 ml-sm"]//*[text()="Message"]')

@then(u'verify displaying the Message input On the Clear Block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="w-50 ml-sm"]//*[@placeholder="Enter Message (Max 1024 characters)"]')))
    assert context.browser.find_element_by_xpath(
        '//div[@class="w-50 ml-sm"]//*[@placeholder="Enter Message (Max 1024 characters)"]')

@then(u'verify displaying the Email Addresses text on the External Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "//label[text()='Email Addresses']")))
    assert context.browser.find_element_by_xpath(
        "//label[text()='Email Addresses']")

@then(u'verify displaying the Enter Email Address input on the External Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//input[@id="tags-separators"]')))
    assert context.browser.find_element_by_xpath(
        '//input[@id="tags-separators"]')

@then(u'verify displaying the hint on the External tab on the External Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "//*[contains(text(), 'Use comma (,) or Space or Enter or click Add button to separate email addresses')]")))
    assert context.browser.find_element_by_xpath(
        "//*[contains(text(), 'Use comma (,) or Space or Enter or click Add button to separate email addresses')]")

@then(u'verify displaying the Import link on the External Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//label[text()='Import']")))
    assert context.browser.find_element_by_xpath(
        "//label[text()='Import']")


@then(u'verify displaying the On Start text on the External Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//div[text()='On Start']")))
    assert context.browser.find_element_by_xpath(
        "//div[text()='On Start']")

@then(u'verify displaying the On Start email input on the External Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//div[@class="w-50 mr-sm"]//textarea[@placeholder="Enter Email Alert Message (Max 1024 characters)"]')))
    assert context.browser.find_element_by_xpath(
        '//div[@class="w-50 mr-sm"]//textarea[@placeholder="Enter Email Alert Message (Max 1024 characters)"]')

@then(u'verify displaying the On Clear text on the External Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//div[text()='On Clear']")))
    assert context.browser.find_element_by_xpath(
        "//div[text()='On Clear']")

@then(u'verify displaying the On Clear email input on the External Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//div[@class="w-50 ml-sm"]//textarea[@placeholder="Enter Email Alert Message (Max 1024 characters)"]')))
    assert context.browser.find_element_by_xpath(
        '//div[@class="w-50 ml-sm"]//textarea[@placeholder="Enter Email Alert Message (Max 1024 characters)"]')

@when(u'user click to QBOX Tab')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//*[text()="QBOX"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//input[@placeholder="Enter and Select Playlist when Start"]')))

@then(u'verify displaying the On Start text on the QBOX Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//*[text()="On Start"]')))
    assert context.browser.find_element_by_xpath(
        '//*[text()="On Start"]')

@then(u'verify displaying dropdown list on the On start block on the QBOX Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//input[@placeholder="Enter and Select Playlist when Start"]')))
    assert context.browser.find_element_by_xpath(
        '//input[@placeholder="Enter and Select Playlist when Start"]')

@when(u'user click to the playlist dropdown on the On start block on the QBOX Tab')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//input[@placeholder="Enter and Select Playlist when Start"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//li[text()="Sorry, no matching options."]')))

@then(u'verify displaying the play list on the On start block on the QBOX Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//li[text()="Sorry, no matching options."]')))
    assert context.browser.find_element_by_xpath(
        '//li[text()="Sorry, no matching options."]')

@then(u'verify displaying the On Clear text on the QBOX Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//*[text()="On Clear"]')))
    assert context.browser.find_element_by_xpath(
        '//*[text()="On Clear"]')

@then(u'verify displaying dropdown list on the On clear block on the QBOX Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//input[@placeholder="Enter and Select Playlist when Clear"]')))
    assert context.browser.find_element_by_xpath(
        '//input[@placeholder="Enter and Select Playlist when Clear"]')

@when(u'user click to the playlist dropdown on the On clear block on the QBOX Tab')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//input[@placeholder="Enter and Select Playlist when Clear"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//li[text()="Sorry, no matching options."]')))

@then(u'verify displaying the play list on the On clear block on the QBOX Tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//li[text()="Sorry, no matching options."]')))
    assert context.browser.find_element_by_xpath(
        '//li[text()="Sorry, no matching options."]')
    #time.sleep(5)

@when(u'user click to Location Tab')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//*[text()="Location"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//*[text()="Default Location"]')))

@then(u'verify displaying the Default Location text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//*[text()="Default Location"]')))
    assert context.browser.find_element_by_xpath(
        '//*[text()="Default Location"]')

@then(u'verify displaying the hint on the Location page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//*[contains(text(), 'Default location will be used when no location can be derived for a MassAlert')]")))
    assert context.browser.find_element_by_xpath(
        "//*[contains(text(), 'Default location will be used when no location can be derived for a MassAlert')]")

@then(u'verify displaying the location dropdown list')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//input[@placeholder="Select Location"]')))
    assert context.browser.find_element_by_xpath(
        '//input[@placeholder="Select Location"]')

@when(u'user click to the select location dropdown list')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//input[@placeholder="Select Location"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//li[@class="vs__dropdown-option vs__dropdown-option--highlight"][1]')))
    #time.sleep(5)

@then(u'verify displaying the location on the select location dropdown list')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//li[@class="vs__dropdown-option vs__dropdown-option--highlight"][1]')))
    assert context.browser.find_element_by_xpath(
        '//li[@class="vs__dropdown-option vs__dropdown-option--highlight"][1]')

@then(u'verify displaying the select location checkbox on the location page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//input[@id="repeat-mass-alert-checkbox"]')))
    assert context.browser.find_element_by_xpath(
        '//input[@id="repeat-mass-alert-checkbox"]')

@then(u'verify displaying the select location text on the location page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//*[contains(text(), 'Allow user to select location for this alert at the time of launching')]")))
    assert context.browser.find_element_by_xpath(
        "//*[contains(text(), 'Allow user to select location for this alert at the time of launching')]")

@then(u'verify displaying the create button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//button[contains(text(), 'Create')]")))
    assert context.browser.find_element_by_xpath(
        "//button[contains(text(), 'Create')]")

@then(u'verify displaying the HTTP Notification text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//*[contains(text(), 'HTTP Notification')]")))
    assert context.browser.find_element_by_xpath(
        "//*[contains(text(), 'HTTP Notification')]")

@then(u'verify displaying the URL text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//*[contains(text(), 'URL')]")))
    assert context.browser.find_element_by_xpath(
        "//*[contains(text(), 'URL')]")

@then(u'verify displaying the URL input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '(//div[@class="form-group"]//input[@class="form-control form-control-sm"])[1]')))
    assert context.browser.find_element_by_xpath(
         '(//div[@class="form-group"]//input[@class="form-control form-control-sm"])[1]')

@then(u'verify displaying the Username text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//*[text()= "Username"]')))
    assert context.browser.find_element_by_xpath(
         '//*[text()= "Username"]')

@then(u'verify displaying the Username input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//div[@class="w-50 mr-sm"]//input[@placeholder="Enter HTTP Notification URL"]')))
    assert context.browser.find_element_by_xpath(
         '//div[@class="w-50 mr-sm"]//input[@placeholder="Enter HTTP Notification URL"]')

@then(u'verify displaying the Password text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//*[text()= "Password"]')))
    assert context.browser.find_element_by_xpath(
         '//*[text()= "Password"]')

@then(u'verify displaying the Password input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//div[@class="w-50 ml-sm"]//input[@placeholder="Enter HTTP Notification URL"]')))
    assert context.browser.find_element_by_xpath(
         '//div[@class="w-50 ml-sm"]//input[@placeholder="Enter HTTP Notification URL"]')

@then(u'verify displaying the MassAlert Name')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//div[@class='col p-0 h-full overflow-hidden']//*[contains(text(), 'Auto Test Name " + str(new_count) +"')]")))
    assert context.browser.find_element_by_xpath(
         "//div[@class='col p-0 h-full overflow-hidden']//*[contains(text(), 'Auto Test Name " + str(new_count) +"')]")

@then(u'verify displaying the MassAlert name on the Subscribe page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//label[contains(text(), 'Auto Test Name "+str(new_count)+"')]")))
    assert context.browser.find_element_by_xpath(
         "//label[contains(text(), 'Auto Test Name "+str(new_count)+"')]")

@then(u'verify displaying the MassAlert description')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//div[@class='col p-0 h-full overflow-hidden']//*[contains(text(), 'Auto Test Description " + str(new_count) +"')]")))
    assert context.browser.find_element_by_xpath(
         "//div[@class='col p-0 h-full overflow-hidden']//*[contains(text(), 'Auto Test Description " + str(new_count) +"')]")

@then(u'verify displaying the MassAlert image')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//div[@class="col p-0 h-full overflow-hidden"]//img[@src="static/img/default_mass_alert.svg"]')))
    assert context.browser.find_element_by_xpath(
         '//div[@class="col p-0 h-full overflow-hidden"]//img[@src="static/img/default_mass_alert.svg"]')

@when(u'user click to the Detail tab')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//div[text() = "Details"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//*[contains(text(), 'Paging Group Notification')]")))

@then(u'verify displaying the Detail tab on the created MassAlert')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//*[contains(text(), 'Paging Group Notification')]")))
    assert context.browser.find_element_by_xpath(
         "//*[contains(text(), 'Paging Group Notification')]")

@when(u'user click to the History tab')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'History')]")
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.ID, "history")))


@then(u'verify displaying the History tab on the created MassAlert')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.ID,
         "history")))
    assert context.browser.find_element_by_id(
         "history")

@then(u'verify displaying the Share With tab on the created MassAlert')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//*[contains(text(), " list is empty")]')))
    assert context.browser.find_element_by_xpath(
         '//*[contains(text(), " list is empty")]')

@when(u'user click to the Subscribers tab')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//*[text()="Subscribers"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "//*[contains(text(), 'External Subscribed users will receive message entered under the External tab')]")))

@then(u'verify displaying the Subscribers tab on the created MassAlert')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//*[contains(text(), 'External Subscribed users will receive message entered under the External tab')]")))
    assert context.browser.find_element_by_xpath(
         "//*[contains(text(), 'External Subscribed users will receive message entered under the External tab')]")

@then(u'verify displaying the Edit button on the created MassAlert')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//button[contains(text(), 'Edit')]")))
    assert context.browser.find_element_by_xpath(
         "//button[contains(text(), 'Edit')]")

@then(u'verify displaying the Delete button on the on the created MassAlert')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//button[contains(text(), 'Delete')]")))
    assert context.browser.find_element_by_xpath(
         "//button[contains(text(), 'Delete')]")

#@then(u'verify displaying the Paging group text for On Start block')
#def step_impl(context):
#    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
#        (By.XPATH,
#         '//div[@class="w-50 mr-sm"]//*[text()="Paging Group"]')))
#    assert context.browser.find_element_by_xpath(
#         '//div[@class="w-50 mr-sm"]//*[text()="Paging Group"]')

@then(u'verify displaying the selected group for On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//div[@class="w-50 mr-sm"]//*[text()="Group for Auto Tests"]')))
    assert context.browser.find_element_by_xpath(
         '//div[@class="w-50 mr-sm"]//*[text()="Group for Auto Tests"]')

@then(u'verify displaying the Message type text for On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//div[@class="w-50 mr-sm"]//*[text()="Message Type"]')))
    assert context.browser.find_element_by_xpath(
         '//div[@class="w-50 mr-sm"]//*[text()="Message Type"]')

@then(u'verify displaying the selected message text for On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//div[@class="w-50 mr-sm"]//*[contains(text(), "text")]')))
    assert context.browser.find_element_by_xpath(
         '//div[@class="w-50 mr-sm"]//*[contains(text(), "text")]')

@then(u'verify displaying the Message text for On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//div[@class="w-50 mr-sm"]//*[text()= "Message"]')))
    assert context.browser.find_element_by_xpath(
         '//div[@class="w-50 mr-sm"]//*[text()= "Message"]')


@then(u'verify displaying the entered message for On Start block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//div[@class='w-50 mr-sm']//*[contains(text(), 'Auto Test Message On Start " + str(new_count) +"')]")))
    assert context.browser.find_element_by_xpath(
        "//div[@class='w-50 mr-sm']//*[contains(text(), 'Auto Test Message On Start " + str(new_count) +"')]")

@then(u'verify displaying the selected group for On Clear block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//div[@class="w-50 ml-sm"]//*[text()="Group for Auto Tests"]')))
    assert context.browser.find_element_by_xpath(
        '//div[@class="w-50 ml-sm"]//*[text()="Group for Auto Tests"]')

@then(u'verify displaying the Message type text for On Clear block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//div[@class="w-50 ml-sm"]//*[text()="Message Type"]')))
    assert context.browser.find_element_by_xpath(
        '//div[@class="w-50 ml-sm"]//*[text()="Message Type"]')

@then(u'verify displaying the selected message text for On Clear block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//div[@class="w-50 ml-sm"]//*[contains(text(), "text")]')))
    assert context.browser.find_element_by_xpath(
        '//div[@class="w-50 ml-sm"]//*[contains(text(), "text")]')

@then(u'verify displaying the Message text for On Clear block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//div[@class="w-50 ml-sm"]//*[text()= "Message"]')))
    assert context.browser.find_element_by_xpath(
        '//div[@class="w-50 ml-sm"]//*[text()= "Message"]')

@then(u'verify displaying the entered message for On Clear block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//div[@class='w-50 ml-sm']//*[contains(text(), 'Auto Test Message On Clear " + str(new_count) +"')]")))
    assert context.browser.find_element_by_xpath(
        "//div[@class='w-50 ml-sm']//*[contains(text(), 'Auto Test Message On Clear " + str(new_count) +"')]")

@then(u'verify displaying the created MassAlert on the Mass Alerts tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         "//div[@class='h-100']//*[contains(text(), 'Auto Test Name " + str(new_count) +"')]")))
    assert context.browser.find_element_by_xpath(
        "//div[@class='h-100']//*[contains(text(), 'Auto Test Name " + str(new_count) +"')]")

@when(u'user click to the QR icon')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//img[@src="static/img/btns/qr.svg"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@class="modal-body"]')))


@then(u'verify displaying Share MassAlert subscriber link')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//div[text()="Share MassAlert subscribing link"]')))
    assert context.browser.find_element_by_xpath(
        '//div[text()="Share MassAlert subscribing link"]')

@then(u'verify displaying the QR code image')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//div[@class="d-flex align-items-center justify-content-center"]//*[contains(@value,"https://noctdips03.quicklert.com/quicklert/subscribemassalert.jsp?massalertId=")]')))
    assert context.browser.find_element_by_xpath(
        '//div[@class="d-flex align-items-center justify-content-center"]//*[contains(@value,"https://noctdips03.quicklert.com/quicklert/subscribemassalert.jsp?massalertId=")]')

@then(u'verify displaying the Mail icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//img[@src="static/img/btns/email.svg"]')))
    assert context.browser.find_element_by_xpath(
        '//img[@src="static/img/btns/email.svg"]')

@then(u'verify displaying the Print icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//img[@src="static/img/btns/print.svg"]')))
    assert context.browser.find_element_by_xpath(
        '//img[@src="static/img/btns/print.svg"]')

@then(u'verify displaying the Facebook icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//img[@src="static/img/btns/facebook.svg"]')))
    assert context.browser.find_element_by_xpath(
        '//img[@src="static/img/btns/facebook.svg"]')

@then(u'verify displaying the Linkedin icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//img[@src="static/img/btns/linkedin.svg"]')))
    assert context.browser.find_element_by_xpath(
        '//img[@src="static/img/btns/linkedin.svg"]')

@then(u'verify displaying the embedded link')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH,
         '//img[@src="static/img/btns/embedded.svg"]')))
    assert context.browser.find_element_by_xpath(
        '//img[@src="static/img/btns/embedded.svg"]')

@when(u'user click to the Share MassAlert link')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//div[text()="Share MassAlert subscribing link"]')
    assert submitButton
    submitButton.click()
    #time.sleep(10)
    #context.browser.switch_to.frame(context.browser.find_element_by_xpath('//iframe[contains(src,noctdips03.quicklert.com/quicklert/subscribemassalert.jsp)]'))
    #windows_mail = context.browser.window_handles[0]
    #print(windows_mail)
    #windows_before = context.browser.window_handles[0]
    #WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
    #    (By.XPATH, '//div[@class="modal-body"]')))
    #time.sleep(5)
@then(u'verify displaying the back button')
def step_impl(context):
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                                 '//*[text()= "Back"]')))
        assert context.browser.find_element_by_xpath(
            '//*[text()= "Back"]')
@given(u'verify displaying the Subscribe MassAlert page')
def step_impl(context):
        context.browser.switch_to.frame(context.browser.find_element_by_xpath(
        '//iframe[contains(src,noctdips03.quicklert.com/quicklert/subscribemassalert.jsp)]'))

    #windows_subscribe = context.browser.window_handles[-1]
        #print(windows_subscribe)
        #context.browser.switch_to_window(windows_subscribe)
        #time.sleep(5)
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
             '//form[@id="subscribedetailsglobal"]')))
        #time.sleep(10)
        assert context.browser.find_element_by_xpath('//form[@id="subscribedetailsglobal"]')

@then(u'verify displaying the Internal block text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//*[text()='Subscribe with your login credentials if you have a quicklert account']")))
    assert context.browser.find_element_by_xpath("//*[text()='Subscribe with your login credentials if you have a quicklert account']")

@then(u'verify displaying the Login Id* text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//*[text()='Login Id *']")))
    assert context.browser.find_element_by_xpath(
        "//*[text()='Login Id *']")


@then(u'verify displaying the Login Id input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//input[@placeholder="Login Id "]')))
    assert context.browser.find_element_by_xpath(
        '//input[@placeholder="Login Id "]')

@then(u'verify displaying the Password* text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//*[text()='Password*']")))
    assert context.browser.find_element_by_xpath(
        "//*[text()='Password*']")
@then(u'verify displaying the Password input for Internal block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//input[@placeholder="Enter password"]')))
    assert context.browser.find_element_by_xpath(
        '//input[@placeholder="Enter password"]')


@then(u'verify displaying the Subscribe button on the Internal block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '(//button[text()= "Subscribe"])[1]')))
    assert context.browser.find_element_by_xpath(
        '(//button[text()= "Subscribe"])[1]')



@then(u'verify displaying the External block text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//*[contains(text(), 'External users subscribe here')]")))
    assert context.browser.find_element_by_xpath(
        "//*[contains(text(), 'External users subscribe here')]")

@then(u'verify displaying the First name* text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//*[text()='First Name *']")))
    assert context.browser.find_element_by_xpath(
        "//*[text()='First Name *']")

@then(u'verify displaying the First name input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//input[@placeholder="Enter First Name"]')))
    assert context.browser.find_element_by_xpath(
        '//input[@placeholder="Enter First Name"]')

@then(u'verify displaying the Last name* text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//*[text()= "First Name *"]')))
    assert context.browser.find_element_by_xpath(
        '//*[text()= "First Name *"]')

@then(u'verify displaying the Last name input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//input[@placeholder="Enter Last Name"]')))
    assert context.browser.find_element_by_xpath(
        '//input[@placeholder="Enter Last Name"]')

@then(u'verify displaying the Email* text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//*[text()= "Email*"]')))
    assert context.browser.find_element_by_xpath(
        '//*[text()= "Email*"]')

@then(u'verify displaying the Email input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//input[@placeholder="Enter email"]')))
    assert context.browser.find_element_by_xpath(
        '//input[@placeholder="Enter email"]')

@then(u'verify displaying the Phone text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//*[text()= "Phone"]')))
    assert context.browser.find_element_by_xpath(
        '//*[text()= "Phone"]')

@then(u'verify displaying the Phone input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//input[@placeholder="Enter 10 digit US/Canada Phone number"]')))
    assert context.browser.find_element_by_xpath(
        '//input[@placeholder="Enter 10 digit US/Canada Phone number"]')

@then(u'verify displaying the Subscribe button on the External block')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '(//button[text()= "Subscribe"])[2]')))
    assert context.browser.find_element_by_xpath(
        '(//button[text()= "Subscribe"])[2]')

@when(u'user click to back button on the Subscribe page')
def step_impl(context):
    context.browser.switch_to.default_content()
    submitButton = context.browser.find_element_by_xpath('//*[text()="Back"]')
    assert submitButton
    submitButton.click()

@then(u'verify displaying the subscription page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//div[@class="col-auto"]')))
    assert context.browser.find_element_by_xpath(
        '//div[@class="col-auto"]')

@when(u'user enter the internal Login id "{loginid}" and Password "{password}"')
def step_impl(context, loginid, password):
    time.sleep(1)
    context.browser.find_element_by_id("loginId").clear()
    assert context.browser.find_element_by_id("loginId")
    context.browser.find_element_by_id("loginId").send_keys(loginid)

    context.browser.find_element_by_id("pwd").clear()
    assert context.browser.find_element_by_id("pwd")
    context.browser.find_element_by_id("pwd").send_keys(password)

@when(u'user click to the subscribe button for internal user')
def step_impl(context):

    submitButton = context.browser.find_element_by_xpath('(//button[text()= "Subscribe"])[1]')
    assert submitButton
    submitButton.click()

@then(u'verify displaying the page about success added internal user')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//span[@id="status"]')))
    assert context.browser.find_element_by_xpath(
        '//span[@id="status"]')

@when(u'user click initiated MassAlert')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//*[contains(text(), 'Auto Test Name "+str(new_count)+" initiated by massalert')]")))

    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'Auto Test Name "+str(new_count)+" initiated by massalert')]")
    assert submitButton
    submitButton.click()


@then(u'verify displaying initiated MassAlert for subscriber')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//*[contains(text(), 'Auto Test Message On Start "+str(new_count)+"')]")))
    assert context.browser.find_element_by_xpath(
        "//*[contains(text(), 'Auto Test Message On Start "+str(new_count)+"')]")

@then(u'verify displaying finished MassAlert for subscriber')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//*[contains(text(), 'Auto Test Message On Clear "+str(new_count)+"')]")))
    assert context.browser.find_element_by_xpath(
        "//*[contains(text(), 'Auto Test Message On Clear "+str(new_count)+"')]")

@when(u'user enter the First Name')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.ID,
                                                                             "fname")))
    context.browser.find_element_by_id("fname").clear()
    assert context.browser.find_element_by_id("fname")
    context.browser.find_element_by_id("fname").send_keys(str(new_count_name) + "_External_First")

@when(u'user enter the Last Name')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.ID,
                                                                             "lname")))
    context.browser.find_element_by_id("lname").clear()
    assert context.browser.find_element_by_id("lname")
    context.browser.find_element_by_id("lname").send_keys(str(new_count_name) + "_External_Last")

@when(u'user enter email')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.ID,
                                                                             "sEmail")))
    context.browser.find_element_by_id("sEmail").clear()
    assert context.browser.find_element_by_id("sEmail")
    context.browser.find_element_by_id("sEmail").send_keys("Autotests.quicklert@gmail.com")

@when(u'user click to the subscribe button for external user')
def step_impl(context):

    submitButton = context.browser.find_element_by_xpath('(//button[text()= "Subscribe"])[2]')
    assert submitButton
    submitButton.click()

@then(u'verify displaying the page about success added external user')
def step_impl(context):
    #context.browser.switch_to_window(context.browser.window_handles[1])
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//span[@id="status"]')))
    assert context.browser.find_element_by_xpath(
        '//span[@id="status"]')

@then(u'verify displaying the page about success added external user after verify')
def step_impl(context):
    context.browser.switch_to_window(context.browser.window_handles[1])
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//span[@id="status"]')))
    assert context.browser.find_element_by_xpath(
        '//span[@id="status"]')

@when(u'user click to Acknowledge checkbox')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "//label[text()='Allow Acknowledge']")))
    submitButton = context.browser.find_element_by_xpath("//label[text()='Allow Acknowledge']")
    assert submitButton
    submitButton.click()

@then(u'verify displaying status checkbox with Acknowledge permission')
def step_impl(context):

    assert context.browser.find_element_by_id('allow-ack-mass-alert-checkbox').is_selected() == True
    assert context.browser.find_element_by_id('allow-reply-mass-alert-checkbox').is_selected() == False
    assert context.browser.find_element_by_id('repeat-mass-alert-checkbox').is_selected() == False

@then(u'verify displaying Acknowledge permission status')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//div[text() = "Allow Acknowledge - "]//div[text() = "ON"]')))
    assert context.browser.find_element_by_xpath(
        '//div[text() = "Allow Acknowledge - "]//div[text() = "ON"]')

@when(u'user click to acknowledge button')
def step_impl(context):
    time.sleep(1)
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//div[@title="Acknowledge"]')))
    submitButton = context.browser.find_element_by_xpath('//div[@title="Acknowledge"]')
    assert submitButton
    submitButton.click()

@when(u'user click to the confirm button for acknowledging permission')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//button[text()="Acknowledge"]')))
    submitButton = context.browser.find_element_by_xpath('//button[text()="Acknowledge"]')
    assert submitButton
    submitButton.click()

@then(u'verify displaying the acknowledge message')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//*[contains(text(), 'Acknowledged by ')]")))
    assert context.browser.find_element_by_xpath(
        "//*[contains(text(), 'Acknowledged by ')]")

@then(u'verify displaying the On Clear message')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//*[contains(text(), 'Auto Test Message On Clear "+str(new_count)+"')]")))
    assert context.browser.find_element_by_xpath(
        "//*[contains(text(), 'Auto Test Message On Clear "+str(new_count)+"')]")
   # time.sleep(1000)

@when(u'user click to Repeat checkbox')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "//label[text()='Allow Acknowledge']")))
    submitButton = context.browser.find_element_by_xpath("//label[text()='Repeat MassAlert']")
    assert submitButton
    submitButton.click()


@then(u'verify displaying status checkbox with Repeat permission')
def step_impl(context):
    assert context.browser.find_element_by_id('allow-ack-mass-alert-checkbox').is_selected() == False
    assert context.browser.find_element_by_id('allow-reply-mass-alert-checkbox').is_selected() == False
    assert context.browser.find_element_by_id('repeat-mass-alert-checkbox').is_selected() == True

@then(u'verify displaying repeat permission status')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//div[text() = "Repeat MassAlert - "]//div[text() = "ON"]')))
    assert context.browser.find_element_by_xpath(
        '//div[text() = "Repeat MassAlert - "]//div[text() = "ON"]')

@when(u'user wait 60 seconds')
def step_impl(context):
    time.sleep(60);

@when(u'user reload page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "//img[@src='static/img/btns/refresh.svg']")))
    submitButton = context.browser.find_element_by_xpath("//img[@src='static/img/btns/refresh.svg']")
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "//div[@style='margin-top: -55px; transition: all 0.3s ease 0s;']")))

@then(u'verify repeat permissions')
def step_impl(context):
    time1 = context.browser.find_element_by_xpath('//div[@class="message-item-body-text-header d-flex align-items-center justify-content-between"]//div[text()="massalert1 massalert1"]/following-sibling::div/div[@class="message-item-body-text-header-date"]').text
    print(time1)
    time2 = context.browser.find_element_by_xpath('//div[@class="date white-space-nowrap ml-xs"]').text
    print(time2)
    assert time1 != time2


@when(u'user click to Reply checkbox')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "//label[text()='Allow Reply']")))
    submitButton = context.browser.find_element_by_xpath("//label[text()='Allow Reply']")
    assert submitButton
    submitButton.click()

@then(u'verify displaying status checkbox with Reply permission')
def step_impl(context):
    assert context.browser.find_element_by_id('allow-ack-mass-alert-checkbox').is_selected() == False
    assert context.browser.find_element_by_id('allow-reply-mass-alert-checkbox').is_selected() == True
    assert context.browser.find_element_by_id('repeat-mass-alert-checkbox').is_selected() == False

@then(u'verify displaying Reply permission status')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//div[text() = "Allow Reply - "]//div[text() = "ON"]')))
    assert context.browser.find_element_by_xpath(
        '//div[text() = "Allow Reply - "]//div[text() = "ON"]')

@when(u'user reply to MassAlert')
def step_impl(context):
    #WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
    #    (By.XPATH, '//div[@class="reply-box"]//input[@type="text"]')))
    time.sleep(1)
    context.browser.find_element_by_xpath('//div[@class="reply-box"]//input[@type="text"]').clear()
    assert context.browser.find_element_by_xpath('//div[@class="reply-box"]//input[@type="text"]')
    context.browser.find_element_by_xpath('//div[@class="reply-box"]//input[@type="text"]').send_keys("Reply Message")


@when(u'user click to Send button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, '//img[@src="static/img/btns/send.svg"]')))
    submitButton = context.browser.find_element_by_xpath('//img[@src="static/img/btns/send.svg"]')
    assert submitButton
    submitButton.click()

@then(u'verify displaying reply message')
def step_impl(context):

    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//div[@id="message-list"]//span[text()="Reply Message"]')))
    assert context.browser.find_element_by_xpath(
        '//div[@id="message-list"]//span[text()="Reply Message"]')

@when(u'user click to Group icon')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//img[@src="static/img/group.svg"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//*[contains(text(),"Groups")]')))


@when(u'user click to the on Start group (Group for Auto Tests)')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath('//*[text()="Group for Auto Tests"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, '//div[@class="d-flex flex-column justify-content-between text-dots"]//*[text()="Group for Auto Tests"]')))


@when(u'user click to the Message tab')
def step_impl(context):

    submitButton = context.browser.find_element_by_xpath('//*[text()="Messages"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, '//*[@class="list"]')))


@then(u'verify displaying message by Massalert on the Group page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//*[contains(text(), 'Auto Test Name " + str(new_count) + "')]")))
    assert context.browser.find_element_by_xpath(
        "//*[contains(text(), 'Auto Test Name " + str(new_count) + "')]")

@then(u'verify displaying message by Massalert for on Start group on the Group page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//*[contains(text(), 'Auto Test Name " + str(new_count) + "')]")))
    assert context.browser.find_element_by_xpath(
        "//*[contains(text(), 'Auto Test Name " + str(new_count) + "')]")

@then(u'verify displaying message by Massalert for on Clear group on the Group page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//*[contains(text(), 'Auto Test Name " + str(new_count) + "')]")))
    assert context.browser.find_element_by_xpath(
        "//*[contains(text(), 'Auto Test Name " + str(new_count) + "')]")

@when(u'user click to created message by Massalert for on Start group on the Group page')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'Auto Test Name " + str(new_count) + "')]")
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'Auto Test Message On Start " + str(new_count) + "')]")))

@when(u'user click to created message by Massalert on the Group page')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[contains(text(), 'Auto Test Name " + str(new_count) + "')]")
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'Auto Test Message On Start " + str(new_count) + "')]")))

@when(u'user click to created message by Massalert for on Clear group on the Group page')
def step_impl(context):
    raise NotImplementedError(u'STEP: When user click to created message by Massalert for on Clear group on the Group page')


@then(u'verify displaying the Feed page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             "//button[text()='All Quicklerts']")))
    assert context.browser.find_element_by_xpath(
        "//button[text()='All Quicklerts']")


@then(u'verify displaying message by Massalert for on Start group on the Feed page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//span[text()="Auto Test Message On Start ' + str(new_count) + '"]')))
    assert context.browser.find_element_by_xpath(
        '//span[text()="Auto Test Message On Start ' + str(new_count) + '"]')

@when(u'user click to the on Start group (Group for Auto Tests 2)')
def step_impl(context):
    ubmitButton = context.browser.find_element_by_xpath('//*[text()="Group for Auto Tests 2"]')
    assert submitButton
    submitButton.click()
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH,
                                        '//div[@class="d-flex flex-column justify-content-between text-dots"]//*[text()="Group for Auto Tests 2"]')))

@then(u'verify displaying message by Massalert for on Clear group on the Feed page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//*[contains(text(),"Auto Test Message On Clear '+str(new_count)+' is cleared now")]')))
    assert context.browser.find_element_by_xpath(
        '//*[contains(text(),"Auto Test Message On Clear '+str(new_count)+' is cleared now")]')

@then(u'verify displaying the Massalert icon on Clear group the Feed page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH,
                                                                             '//div[@class="d-flex contact-info overflow-hidden"]//img[@src="static/img/default_mass_alert.svg"]')))
    assert context.browser.find_element_by_xpath(
        '//div[@class="d-flex contact-info overflow-hidden"]//img[@src="static/img/default_mass_alert.svg"]')



