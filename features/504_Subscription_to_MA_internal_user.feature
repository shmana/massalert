Feature: Subscription to MA internal user

  Scenario: Login to the MassAlert page as user 1
    Given sign in opened page
    When user entered his username "massalert" and password "qwe123123"
    When user click on Log In button
    Then user should be routed to home page

  Scenario: user create MassAlert
    Given displaying the automated user
      When user click to MassAlerts icon
      And user click to create MassAlert icon
      And user enter the MassAlert Name
      And user enter the MassAlert Description
      And user enter the MassAlert Pin code
      And user click to Internal Tab
      And user click to the Paging group dropdown list on the On Start block
      And user select the Group for Auto Tests on the On Start block
      And user enter the Message to the Message input on the On Start block
      And user click to the Paging group dropdown list on the On the Clear Block
      And user select the Group for Auto Tests On the Clear Block
      And user enter the Message to the Message input On the Clear Block
      And user click to API's Tab
      And user click to create button
        Then verify displaying the created MassAlert

  Scenario: trigger to QR code
    Given displaying the automated user
    When user click to selected MassAlert
      And user click to the Share With tab
      And user click to the QR icon
      And user click to the Share MassAlert link
        Then verify displaying the subscription page

  Scenario: Subscription page for internal user
    Given verify displaying the Subscribe MassAlert page
      When user enter the internal Login id "massalert1" and Password "qwe123123"
      And user click to the subscribe button for internal user
        Then verify displaying the page about success added internal user
      When user click to back button on the Subscribe page
    Given displaying the automated user

   Scenario: User Start MassAlert
    Given displaying the automated user
      When user click to MassAlerts icon
      And user click to selected MassAlert
      And user click to start MassAlert
      And user enter the Pin code MassAlert
      And user click to the Start button
        Then verify displaying pop up window about successful started MassAlert
      When user click to the Close button
        Then verify MassAlert has started

    Scenario: Login to the MassAlert page as user 2
    Given sign in opened page
        When user entered his username "massalert1" and password "qwe123123"
        When user click on Log In button
          Then user should be routed to home page

    Scenario:  Subscriber verify displaying started MassAlert
      Given displaying the automated user
        When user click to Feed icon
        And user click initiated MassAlert
          Then verify displaying initiated MassAlert for subscriber

    Scenario: Login to the MassAlert page as user 1
      Given sign in opened page
        When user entered his username "massalert" and password "qwe123123"
        When user click on Log In button
          Then user should be routed to home page

    Scenario: User Stop MassAlert
      Given displaying the automated user
        When user click to MassAlerts icon
        And user click to selected MassAlert
        And user click to stop MassAlert
        And user enter the Pin code MassAlert
        And user click to the Stop button
          Then verify MassAlert has finished

    Scenario: Login to the MassAlert page as user 2
      Given sign in opened page
        When user entered his username "massalert1" and password "qwe123123"
        When user click on Log In button
          Then user should be routed to home page

    Scenario:  Subscriber verify displaying finished MassAlert
      Given displaying the automated user
        When user click to Feed icon
        And user click initiated MassAlert
          Then verify displaying finished MassAlert for subscriber